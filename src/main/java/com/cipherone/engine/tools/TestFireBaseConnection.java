package com.cipherone.engine.tools;

import java.util.Random;

import com.cipherone.engine.commons.Annotation;
import com.cipherone.engine.commons.PerformanceScore;
import com.cipherone.engine.firebase.FireBaseConnector;

public class TestFireBaseConnection {
   public static void main(String[] args) {
	   FireBaseConnector fconn = new FireBaseConnector("https://scorching-heat-1149.firebaseio.com");
	   
	   String topic = "TEST";
	   
	   boolean currentStatus = topic.equalsIgnoreCase("ReadyForML"); // fconn.checkStatus(topic);
	   
	   int nLimit = -1;
	   
	   Random rand = new Random();
	   for (int i=0; i<nLimit; i++) {
//		   PerformanceScore score = new PerformanceScore();
//		   score.setId(Integer.toString(i));
//		   score.setTruePositives(rand.nextInt(100));
//		   score.setFalsePositives(rand.nextInt(100));
//		   score.setFalseNegatives(rand.nextInt(100));
//		   score.completeScores();
//		   
//		   fconn.addMetric("TEST", score);
		   
		   double score = Math.random();
		   boolean verified = false;
		   String status = "-";
		   if(score >= 0.5) status = "+";
		   if(score > 0.9) verified = true;
		   
		   Annotation annotation = new Annotation();
		   annotation.setId(Integer.toString(i));
		   annotation.setAnnotation(status);
//		   annotation.setScore(score);
//		   annotation.setVerified(verified);
		   
		   fconn.addRecord(topic, annotation);
	   }
	   
	   fconn.updateStatus(topic, "ReadyForML");
	   fconn.close();
	   
//	   // Attach an listener to read the data at our posts reference
//	   dataRef.addValueEventListener(new ValueEventListener() {
//	       public void onDataChange(DataSnapshot snapshot) {
//	           System.out.println("There are " + snapshot.getChildrenCount() + " blog posts");
//	           for (DataSnapshot postSnapshot: snapshot.getChildren()) {
//	             Annotation post = postSnapshot.getValue(Annotation.class);
//	             System.out.println(post.getId() + " - " + post.getAnnotation());
//	           }
//	       }
//	       public void onCancelled(FirebaseError firebaseError) {
//	           System.out.println("The read failed: " + firebaseError.getMessage());
//	       }
//	   });
//	   
//	   dataRef.addChildEventListener(new ChildEventListener() {
//		    // Retrieve new posts as they are added to the database
//		    public void onChildAdded(DataSnapshot snapshot, String previousChildKey) {
//		        Annotation newPost = snapshot.getValue(Annotation.class);
//		        System.out.println("Id: " + newPost.getId());
//		        System.out.println("Annotation: " + newPost.getAnnotation());
//		    }
//		    
//		    public void onChildChanged(DataSnapshot snapshot, String previousChildKey) {
//		        String title = (String) snapshot.child("Annotation").getValue();
//		        System.out.println("The updated post title is " + title);
//		    }
//		    
//		    public void onChildRemoved(DataSnapshot snapshot) {
//		        String title = (String) snapshot.child("Id").getValue();
//		        System.out.println("The blog post titled " + title + " has been deleted");
//		    }
//		    
//		    public void onChildMoved(DataSnapshot snapshot, String message) {
//		        String title = (String) snapshot.child("Id").getValue();
//		        System.out.println("The blog post titled " + title + " has been deleted");
//		    }
//		    public void onCancelled(FirebaseError firebaseError) {
//		    	System.out.println("The read failed: " + firebaseError.getMessage());
//		    }
//		});
   }
}