package com.cipherone.engine.tools;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;

import org.elasticsearch.action.admin.indices.create.CreateIndexRequestBuilder;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequestBuilder;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsResponse;
import org.elasticsearch.action.get.GetRequestBuilder;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequestBuilder;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.xcontent.XContentBuilder;

public class RunDocumentIndexing {
	private static Client getClient() throws UnknownHostException {
//		Client client = NodeBuilder.nodeBuilder()
//                .client(true)
//                .node()
//                .client();
//		
//		return client;
        TransportClient transportClient = TransportClient.builder().build(); // (settings);
        transportClient = transportClient.addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("localhost"), 9300))
        		.addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("localhost"), 9200));
        return (Client) transportClient;
        
    }
		
	public static void main(String[] args) throws IOException, InterruptedException {
		final Client client = getClient();
	    // Create Index and set settings and mappings
	    final String indexName = "test";
	    final String documentType = "tweet";
	    final String documentId = "2";
	    final String fieldName = "foo1";
	    final String value = "bar1";
	
	    final IndicesExistsResponse res =  client.admin().indices().prepareExists(indexName).execute().actionGet();
	    if (res.isExists()) {
	        DeleteIndexRequestBuilder delIdx = client.admin().indices().prepareDelete(indexName);
	        delIdx.execute().actionGet();
	    }
	
	    final CreateIndexRequestBuilder createIndexRequestBuilder = client.admin().indices().prepareCreate(indexName);
	
	    // MAPPING GOES HERE	
	    final XContentBuilder mappingBuilder = jsonBuilder().startObject().startObject(documentType)
                .startObject("_ttl").field("enabled", "true").field("default", "1s").endObject().endObject()
                .endObject();
        System.out.println(mappingBuilder.string());
        createIndexRequestBuilder.addMapping(documentType, mappingBuilder);

        // MAPPING DONE
        createIndexRequestBuilder.execute().actionGet();	
        
	    // Add documents
	    final IndexRequestBuilder indexRequestBuilder = client.prepareIndex(indexName, documentType, documentId);
	    // build json object
	    final XContentBuilder contentBuilder = jsonBuilder().startObject().prettyPrint();
	    contentBuilder.field(fieldName, value);
	    
	    indexRequestBuilder.setSource(contentBuilder);
	    indexRequestBuilder.execute().actionGet();
	
	    // Get document
	    System.out.println(getValue(client, indexName, documentType, documentId, fieldName));
	}
		
	protected static String getValue(final Client client, final String indexName, final String documentType,
            final String documentId, final String fieldName) {
        final GetRequestBuilder getRequestBuilder = client.prepareGet(indexName, documentType, documentId);
        getRequestBuilder.setFields(new String[] { fieldName });
        final GetResponse response2 = getRequestBuilder.execute().actionGet();
        if (response2.isExists()) {
            final String name = response2.getField(fieldName).getValue().toString();
            return name;
        } else {
            return "ID_NOT_FOUND";
        }
    }

}
