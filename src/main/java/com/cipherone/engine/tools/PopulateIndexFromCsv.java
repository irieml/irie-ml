package com.cipherone.engine.tools;

import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

import org.elasticsearch.common.xcontent.XContentBuilder;

import com.cipherone.engine.utils.ElasticUtils;

public class PopulateIndexFromCsv
{
	public static String[] fields;
	public static String header = "{\"index\":{\"_index\":\"documents\",\"_type\":\"doc\",\"_id\":"; // \"ID\"}";
	
    public static void main(String[] args) throws Exception
    {
 	   String sourceFile = args[0];
 	   
 	   ElasticUtils testClient = new ElasticUtils();
		
 	   BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(sourceFile)));
	   String[] targeted = new String[2];
	   targeted[0] = "title";
	   targeted[1] = "text";

 	   String line = br.readLine();
 	   fields = line.replaceAll("\"", "").split("\\t");
 	   int nFields = fields.length;
 	   int found = 0;
 	   String sb = "";
 	   
 	   int segment = 0;
 	   int docCount = 0;
 	   
 	   String jsonFile = "subset-" + segment + ".json";
 	   FileWriter outJson = new FileWriter(jsonFile);
 	   
 	   while((line = br.readLine()) != null) {
 		   String clean;
 		   if(!line.contains("\"ABS\""))
 			   clean = line.replaceAll("\\t", " ");
 		   else
 			   clean = line;
 		   String[] tmp = clean.split("\\t");
 		   found += tmp.length - 1;
 		   
 		   if(found > nFields) {
 			   docCount ++;
 			   
 			   String[] values = sb.replaceAll("\"", "").split("\\t");
 			   String docId = values[0];
 			   outJson.write(header + "\"" + docId + "\"}" + '\n');
 			   outJson.write(testClient.getDocument(docId) + '\n');
 			   if(docCount > 2000) {
 				   docCount = 0;
 				   segment ++;
 				   outJson.close();
 				   jsonFile = "subset-" + segment + ".json";
 				   outJson = new FileWriter(jsonFile);
 			   }
 			   

 			  // populateAWS(docId, testClient.getDocument(docId));
// 			   String valueString = doc2Json(values);
// 			   testClient.save(docId, valueString);
// 			   testClient.update(docId, "fv", testClient.getTermVector(docId, targeted));
 			   
 			   sb = "";
 			   found = tmp.length - 1;
 		   }
 		   sb += clean + " ";
 	   }
 	   
 	   String[] values = sb.replaceAll("\"", "").split("\\t");
 	   String docId = values[0];
	   outJson.write(header + "\"" + docId + "\"}" + '\n');
	   outJson.write(testClient.getDocument(docId) + '\n');
 	   // populateAWS(docId, testClient.getDocument(docId));
 	   outJson.close();
//	   String valueString = doc2Json(values);
//	   testClient.save(docId, valueString);
//	   testClient.update(docId, "fv", testClient.getTermVector(docId, targeted));
	   
 	   
 	   br.close();
    }
    
    public static String doc2Json(String[] values) throws IOException {
		XContentBuilder builder = jsonBuilder()
			    .startObject()
			        .field("doc_id", values[0])
			        .field("title", values[1].trim())
			        .field("text", values[3].trim())
			        .field("doc_type", "doc")
			        .field("sa", "")
			        .field("fv", "")
//			        .field("m_label", "")
//			        .field("h_label", "")
//			        .field("annotation", "")
			        .field("fy", "2013")
			    .endObject();

		return builder.string();
    }
    
    public static void populateAWS(String docId, String jsonString) throws IOException {
    	  ProcessBuilder pb = new ProcessBuilder(
    	            "curl",
    	            "-XPUT",
    	            "https://search-datasci-lwqxdpyfhmpvb2fievkhwgutca.us-west-2.es.amazonaws.com/documents/doc/" + docId,
    	            "-d",
    	            jsonString);
    	  Process p = pb.start();
//    	  InputStreamReader isr = new InputStreamReader(p.getInputStream());
//          BufferedReader br = new BufferedReader(isr);
//          String line;
//          while ((line = br.readLine()) != null) {
//              System.out.println(line);
//          }
    	  p.destroy();
    }
}
