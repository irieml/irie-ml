package com.cipherone.engine.tools;

import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

import org.elasticsearch.common.xcontent.XContentBuilder;

public class ToJson
{
	public static String header;
	public static String[] fields;
    public static void main(String[] args) throws Exception
    {
 	   String sourceFile = args[0];
 	   String jsonFile = args[1];
		
 	   header = "{\"index\":{\"_index\":\"documents\",\"_type\":\"doc\",\"_id\":"; // \"ID\"}";
 	   // System.out.println(header);
 	   
 	   /*
 	    * 
 	    * index :
    analysis :
        analyzer :
            standard :
                type : standard
                stopwords : [stop1, stop2]
            myAnalyzer1 :
                type : standard
                stopwords : [stop1, stop2, stop3]
                max_token_length : 500
            # configure a custom analyzer which is
            # exactly like the default standard analyzer
            myAnalyzer2 :
                tokenizer : standard
                filter : [standard, lowercase, stop]
        tokenizer :
            myTokenizer1 :
                type : standard
                max_token_length : 900
            myTokenizer2 :
                type : keyword
                buffer_size : 512
        filter :
            myTokenFilter1 :
                type : stop
                stopwords : [stop1, stop2, stop3, stop4]
            myTokenFilter2 :
                type : length
                min : 0
                max : 2000
                
 	    */
 	   
 	   /*
 	    * mapping
 	    * {"nih":{"mappings":{"grant":{"properties":
 	    * 	{
 	    * 		"ABS":{"type":"string", "analyzer": "standard"},
 	    * 		"APPL_ID":{"type":"string", "index": "not_analyzed"},
 	    * 		"PROJECT_TITLE":{"type":"string", "analyzer": "standard"},
 	    * 		"annotation":{"type":"string", "index": "not_analyzed"},
 	    * 		"doc_type":{"type":"string", "index": "not_analyzed"},
 	    * 		"fv":{"type":"string", "index": "not_analyzed"},
 	    * 		"fy":{"type":"string", "index": "not_analyzed"},
 	    * 		"sa":{"type":"string", "analyzer": "standard"},
 	    * 		"create_date":{"type":"date"}, "index": "not_analyzed"}}}}
 	    */

 	   /* stop word filtering
 	    * 
 	    * PUT /my_index
 	    * {
 	    * 	"settings": {
 	    * 		"analysis": {
 	    * 			"filter": {
 	    * 				"my_stop": {
 	    * 					"type":       "stop",
 	    * 					"stopwords":  "_english_"
 	    * 				}
 	    * 			}
 	    * 		}
 	    * 	}
 	    * }
 	    */
 	   
 	   /*
 	    * 
 	    * curl -XPUT 'localhost:9200/myindex/_settings' -d '{
  "analysis" : {
    "analyzer":{
      "content":{
        "type":"custom",
        "tokenizer":"whitespace"
      }
    }
  }
}'
 	    */
 	   
 	   /* indexing analyzer
 	    * {
 	    * 	"index" : {
 	    * 		"analysis" : {
 	    * 			"analyzer" : {
 	    * 				"my_analyzer" : {
 	    * 					"tokenizer" : "standard",
 	    * 					"filter" : ["standard", "lowercase", "my_stemmer"]
 	    * 				}
 	    * 			},
 	    * 			"filter" : {
 	    * 				"my_stemmer" : {
 	    * 					"type" : "stemmer",
 	    * 					"name" : "english"
 	    * 				}
 	    * 			}
 	    * 		}
 	    * 	}
 	    * }
 	    */

 	   BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(sourceFile)));
 	   FileWriter outJson = new FileWriter(jsonFile);

 	   String line = br.readLine();
 	   fields = line.replaceAll("\"", "").split("\\t");
 	   int nFields = fields.length;
 	   int found = 0;
 	   String sb = "";
 	   while((line = br.readLine()) != null) {
 		   String clean;
 		   if(!line.contains("\"ABS\""))
 			   clean = line.replaceAll("\\t", " ");
 		   else
 			   clean = line;
 		   String[] tmp = clean.split("\\t");
 		   found += tmp.length - 1;
 		   
 		   if(found > nFields) {
 			   String[] values = sb.replaceAll("\"", "").split("\\t");
 			   String valueString = doc2Json(values);
 			   outJson.write(header + "\"" + values[0] + "\"}" + '\n');
 			   outJson.write(valueString + '\n');
 			   
 			   sb = "";
 			   found = tmp.length - 1;
 		   }
 		   sb += clean + " ";
 	   }
 	   String[] values = sb.replaceAll("\"", "").split("\\t");
	   String valueString = doc2Json(values);
	   outJson.write(header + "\"" + values[0] + "\"}" + '\n');
	   outJson.write(valueString + '\n');
 	   
 	   br.close();
 	   outJson.close();
    }
    
    public static String doc2Json(String[] values) throws IOException {
		XContentBuilder builder = jsonBuilder()
			    .startObject()
			        .field("doc_id", values[0])
			        .field("title", values[1].trim())
			        .field("text", values[3].trim())
			        .field("doc_type", "doc")
			        .field("sa", "")
			        .field("fv", "")
			        .field("m_label", "")
			        .field("h_label", "")
			        .field("annotation", "")
			        .field("fy", "2013")
			    .endObject();

		return builder.string();
    }
}
