package com.cipherone.engine.tools;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import org.apache.log4j.Logger;

import com.cipherone.engine.commons.Annotation;
import com.cipherone.engine.commons.PerformanceScore;
import com.cipherone.engine.firebase.FireBaseConnector;
import com.cipherone.engine.svm.DataInstance;
import com.cipherone.engine.svm.DataSet;
import com.cipherone.engine.svm.OVA;
import com.cipherone.engine.svm.OVA_Parameters;
import com.cipherone.engine.svm.SVMModel;
import com.cipherone.engine.svm.ModelEngine;
import com.cipherone.engine.svm.ModelInfo;
import com.cipherone.engine.svm.SVMUtility;
import com.cipherone.engine.utils.ElasticUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gson.Gson;

public class BackendDaemon {
//public class BackendDaemon extends Thread {
	final static String elasticurl = "https://search-datasci-lwqxdpyfhmpvb2fievkhwgutca.us-west-2.es.amazonaws.com";
	final static String fireurl = "https://iriedata.firebaseio.com";
	public final static int nDocsForScreening = 200;
	
	public static void main(String[] args) throws InterruptedException, ExecutionException, IOException {
//		System.out.println("Main Method Entry");
		
//		String query = ElasticUtils.buildQuery("Lung Cancer", 0.4, 0.4, 0.6);
		run();
 
//		BackendDaemon t = new BackendDaemon();
//		t.setDaemon(true);
//		// When false, (i.e. when it's a user thread), the Worker thread
//		// continues to run.
//		// When true, (i.e. when it's a daemon thread), the Worker thread
//		// terminates when the main thread terminates.
//		t.start();
// 
//		try {
//			Thread.sleep(3000);
//		} catch (InterruptedException x) {
//		}
 
		System.out.println("Main Method Exit");
	}
 
	public static void run() throws IOException {
		System.out.println("run Method Entry");
		
		FireBaseConnector fconn = new FireBaseConnector(fireurl);
 
		try {
			while (true) {				
				try {
					fconn.extractStatus();
					
					for (String topic : fconn.getReviewStatus().keySet()) {
						String status = fconn.getReviewStatus().get(topic);

						if(! status.equalsIgnoreCase(FireBaseConnector.LEARN) &&
							! status.equalsIgnoreCase(FireBaseConnector.RETRIEVE))
							continue;
		
						fconn.extractAnnotations(topic);
						DataSet trainset = fconn.getReviewedDocuments().get(topic);
						if(trainset == null) continue;
						fconn.updateStatus(topic, FireBaseConnector.LOCKED);

						SVMModel svmModel= buildModel(topic, trainset);
						
						String newStatus;
						double threshold = 0.0;
						int limit = nDocsForScreening;
						boolean retrieveAll;
						
						if(status.equalsIgnoreCase(FireBaseConnector.LEARN)) {
							newStatus = FireBaseConnector.REVIEW;
							retrieveAll = false;
						} else {
							// newStatus = FireBaseConnector.DONE;
							newStatus = FireBaseConnector.REVIEW;
							retrieveAll = true;
							limit = nDocsForScreening * 5;
						}
						
						List<Annotation> documents = retrieve(topic, trainset, svmModel, retrieveAll, threshold, limit);
						
						// save retrieved documents to csv file.
//						if(retrieveAll) {
//							Gson gson = new Gson();
//							String json = gson.toJson(documents);
//
//							String savedRetrievedFile = topic + "-RetrievedDocuments.json";
//							String savedModelFile = topic + ".model";
//							try {
//								FileWriter retrieved = new FileWriter(savedRetrievedFile);
//								retrieved.write(json);
//								retrieved.close();
//
//								FileWriter modelFile = new FileWriter(savedModelFile);
//								modelFile.write(svmModel.toJson());
//								modelFile.close();
//
//							} catch (IOException e) {
//								e.printStackTrace();
//							}
//
//							documents.clear();
//						}
						
						appendTrainingData(documents, trainset);
						updateDatabase(fconn, topic, documents, svmModel.getInfo().getPerformance(), newStatus);
					}
					
//					fconn.close();
					Thread.sleep(3000);
				} catch (InterruptedException x) {
				}
 
				// System.out.println("In run method.." + Thread.currentThread());
			}
		} finally {
			System.out.println("run Method Exit");
		}
	}
	
	private static SVMModel buildModel(String topic, DataSet trainset) throws InterruptedException {
		System.out.println("Working on " + topic);
		
		// Run SVM training/screening
		SVMModel svmModel = null;
		try {
			svmModel = runSVM(trainset, topic);
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		PerformanceScore performance;
		if(svmModel != null) {
			performance = svmModel.getInfo().getPerformance();
		} else {
			performance = new PerformanceScore();
			performance.setRecall(1.0);
			performance.setPrecision(0.5);
			performance.setfScore(0.67);
			
			ModelInfo info = new ModelInfo();
			info.setPerformance(performance);
			
			svmModel = new SVMModel(null, info);
		}
		
		return svmModel;
	}
	
	private static void updateDatabase(FireBaseConnector fconn, String topic, List<Annotation> documents, PerformanceScore performance, String status) {
		fconn.updateAnnotation(topic, documents);
		fconn.updatePerformanceScore(topic, performance);
		fconn.updateStatus(topic, status);
	}
	
	private static List<Annotation> retrieve(String topic, DataSet trainset, SVMModel svmModel, boolean all, double threshold, int limit) throws InterruptedException, IOException {
		System.out.println("Working on " + topic);
		Set<String> annotated = DataSet.createNoteIDToInstanceIndex(trainset).keySet();
		
		HashMap<String, Annotation> scores = screeningCandidates(svmModel, topic, all, threshold, limit);

		List<Annotation> documents = Lists.newArrayList();
		for (String docId : scores.keySet()) {
			if(! annotated.contains(docId)) {
				documents.add(scores.get(docId));
			}
		}
		
		return documents;
	}
	
	private static void appendTrainingData(List<Annotation> documents, DataSet trainset) {
		// attach the annotated documents in the end
		for (DataInstance inst : trainset.getInstances()) {
			documents.add(new Annotation(Integer.parseInt(inst.getName()), FireBaseConnector.labelMap.inverse().get(inst.getAnnotation().keySet().iterator().next())));
		}
	}

	private static SVMModel runSVM(DataSet trainset, String topic) throws InterruptedException, ExecutionException, IOException {
		// System.out.println("runSVM " + Thread.currentThread().getName());
    	// set OVA parameters
    	OVA_Parameters param = OVA_Parameters.getDefault();
    	param.setFileRoot(topic.replaceAll("\\s+", "-"));
    	
    	ElasticUtils elastic = new ElasticUtils(elasticurl);
		
		//  go get fvs from elasticsearch by docId
		for (DataInstance instance: trainset) {
			String fvString = elastic.getValue(instance.getName());
			String formatedString = SVMUtility.formFvs(SVMUtility.normalizeFvs(fvString, param.getNormalizationScheme()), true, null);
			instance.loadFvs(formatedString);
		}
		
		elastic.close();
		
		OVA engine = new OVA();
		engine.setParams(param);
		try {
			engine.setDataSets(trainset, null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// trainset.fixFeatureSpace();
		engine.runTraining();
		
		if(trainset.getInstances().size() <= 30) {
			engine.runCrossValidation(trainset.getInstances().size());
		} else {
			engine.runCrossValidation(10);
		}
		PerformanceScore performance = engine.getPerformance();
		
		ModelInfo info = new ModelInfo();
		info.setPerformance(performance);
		info.setNumNotesUsed(trainset.getInstances().size());
		info.setNumNotesTested(trainset.getInstances().size());
		
	   	ModelEngine model = engine.getModels().get("GOOD");
	   	SVMModel svmModel = null;
    	if(model != null) {
    		svmModel = new SVMModel(model, info);
    	} else {
    		System.out.println("No model was built for " + topic + ", most likely due to lack of training instances");
    	}
    	
    	return svmModel;
	}
	
	public static HashMap<String, Annotation> screeningCandidates(SVMModel svmModel, String topic, boolean all, double threshold, int limit) throws IOException {
		HashMap<String, Annotation> scores = Maps.newHashMap();

    	ElasticUtils elastic = new ElasticUtils(elasticurl);

    	if(svmModel == null || svmModel.getEngine() == null) {
    		scores = elastic.search(topic, nDocsForScreening);
    	} else {
			try {
				scores = elastic.screening(svmModel.getEngine(), all, threshold, limit);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    	
		elastic.close();
		
		return scores;
	}
}
