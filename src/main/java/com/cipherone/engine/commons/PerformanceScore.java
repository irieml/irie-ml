package com.cipherone.engine.commons;

public class PerformanceScore implements Comparable<PerformanceScore> {
	private int truePositives;
	private int falsePositives;
	private int falseNegatives;
	private int trueNegatives;
	
	private double threshold;
	private double recall;
	private double precision;
	private double fScore;

	public PerformanceScore() {
		recall = 0.0;
		precision = 0.0;
		fScore = 0.0;
	}
	
	public void completeScores() {
		if(truePositives == 0) {
			recall = (double) 0.0;
			precision = (double) 0.0;
			fScore = (double) 0.0;
		} else {
			recall = ((double)truePositives) / (double)(truePositives + falseNegatives);
			precision = ((double)truePositives) / (double)(truePositives + falsePositives);
			fScore = (double) (2.0 * recall * precision / (double)(recall + precision));
		}
	}
	
	public int compareTo(PerformanceScore aThat) {
		int status = 0;
		if(fScore == aThat.fScore) {
			if(truePositives == 0) {
				if(falsePositives < aThat.falsePositives) {
					status = 1;
				} else if(falsePositives > aThat.falsePositives) {
					status = -1;
				}
			} else {
				if(precision > aThat.precision) {
					status = 1;
				} else if(precision < aThat.precision) {
					status = -1;
				} else {
					if(recall > aThat.recall) {
						status = 1;
					} else if(recall < aThat.recall) {
						status = -1;
					}
				}
			}
		}
		else {
			if(fScore > aThat.fScore) {
				status = 1;
			} else {
				status = -1;
			}
		}
		
		return status;
	}
	
	@Override
	public PerformanceScore clone() {
		PerformanceScore score = new PerformanceScore();
		
		score.truePositives = truePositives;
		score.falsePositives = falsePositives;
		score.falseNegatives = falseNegatives;
		score.recall = recall;
		score.precision = precision;
		score.fScore = fScore;
		score.threshold = threshold;
		
		return score;
	}
	
	public void takeAverage(double divider) {
		truePositives /= divider;
		falsePositives /= divider;
		falseNegatives /= divider;
		trueNegatives /= divider;
		recall /= divider;
		precision /= divider;
		fScore /= divider;
	}

	public void update(PerformanceScore individual) {
		truePositives += individual.getTruePositives();
		falsePositives += individual.getFalsePositives();
		falseNegatives += individual.getFalseNegatives();
		trueNegatives += individual.getTrueNegatives();
		recall += individual.getRecall();
		precision += individual.getPrecision();
		fScore += individual.getfScore();
	}
	
	public String toOutputString() {
		StringBuilder scoreString = new StringBuilder();
		scoreString.append(Integer.toString(truePositives) + '\t');
		scoreString.append(Integer.toString(falsePositives) + '\t');
		scoreString.append(Integer.toString(falseNegatives) + '\t');
		scoreString.append(Integer.toString(trueNegatives) + '\t');
		scoreString.append(String.format("%.3f", recall) + '\t');
		scoreString.append(String.format("%.3f", precision) + '\t');
		scoreString.append(String.format("%.3f", fScore) + '\t');
				
		scoreString.append(System.getProperty("line.separator"));
		
		return scoreString.toString();
	}
	
	public int getTruePositives() {
		return truePositives;
	}
	
	public void setTruePositives(int number) {
		truePositives = number;
	}

	public int getFalsePositives() {
		return falsePositives;
	}
	
	public void setFalsePositives(int number) {
		falsePositives = number;
	}
	
	public int getFalseNegatives() {
		return falseNegatives;
	}
	
	public void setFalseNegatives(int number) {
		falseNegatives = number;
	}

	public int getTrueNegatives() {
		return trueNegatives;
	}
	
	public void setTrueNegatives(int number) {
		trueNegatives = number;
	}
	
	public double getRecall() {
		return recall;
	}
	
	public void setRecall(double value) {
		recall = value;
	}

	public double getPrecision() {
		return precision;
	}
	
	public void setPrecision(double value) {
		precision = value;
	}

	public double getThreshold() {
		return threshold;
	}

	public void setThreshold(double threshold) {
		this.threshold = threshold;
	}

	public double getfScore() {
		return fScore;
	}

	public void setfScore(double fScore) {
		this.fScore = fScore;
	}
}
