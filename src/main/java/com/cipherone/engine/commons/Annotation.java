package com.cipherone.engine.commons;

public class Annotation {
	private String id;
	private String annotation;
	//private double score = -999.0;
	//private boolean verified = false;
	
	public Annotation() {}

	public Annotation(int _id, String _ann) {
		id = Integer.toString(_id);
		annotation = _ann;
	}
	
	public String toString() {
		return "Id:" + id + "  Annotation:" + annotation;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getAnnotation() {
		return annotation;
	}
	public void setAnnotation(String annotation) {
		this.annotation = annotation;
	}

//	public double getScore() {
//		return score;
//	}
//
//	public void setScore(double score) {
//		this.score = score;
//	}
//
//	public boolean isVerified() {
//		return verified;
//	}
//
//	public void setVerified(boolean verified) {
//		this.verified = verified;
//	}
}
