package com.cipherone.engine.commons;

import java.util.HashMap;

public class ThresholdProfile {
	HashMap<String, Double> thresholds = null;
	HashMap<String, Double> captureRates = null;
	HashMap<String, PerformanceScore> performances = null;
	
	public ThresholdProfile() { init(); }
	
	private void init() {
		thresholds = new HashMap<String, Double>(4);
		thresholds.put("B", 99.0);
		thresholds.put("A", 99.0);
		thresholds.put("C", 99.0);
		thresholds.put("N", 99.0);
		
		captureRates = new HashMap<String, Double>(4);
		captureRates.put("B", 0.6);
		captureRates.put("A", 0.7);
		captureRates.put("C", 0.5);
		captureRates.put("N", 0.0);

		performances = new HashMap<String, PerformanceScore>(4);
		performances.put("B", new PerformanceScore());
		performances.put("A", new PerformanceScore());
		performances.put("C", new PerformanceScore());
		performances.put("N", new PerformanceScore());
	}
	
	public void setThreshold(String key, double value) {
		thresholds.put(key, value);
	}
	public double getThreshold(String key) {
		return thresholds.get(key);
	}
	public void setCaptureRate(String key, double value) {
		captureRates.put(key, value);
	}
	public double getCaptureRate(String key) {
		return captureRates.get(key);
	}
	public void setPerformance(String key, PerformanceScore value) {
		performances.put(key, value);
	}
	public PerformanceScore getPerformance(String key) {
		return performances.get(key);
	}

	public HashMap<String, Double> getThresholds() {
		return thresholds;
	}

	public HashMap<String, Double> getCaptureRates() {
		return captureRates;
	}

	public HashMap<String, PerformanceScore> getPerformances() {
		return performances;
	}

	public void setThresholds(HashMap<String, Double> thresholds) {
		this.thresholds = thresholds;
	}

	public void setCaptureRates(HashMap<String, Double> captureRates) {
		this.captureRates = captureRates;
	}

	public void setPerformances(HashMap<String, PerformanceScore> performances) {
		this.performances = performances;
	}
}
