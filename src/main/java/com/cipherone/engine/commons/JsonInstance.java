package com.cipherone.engine.commons;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.DateSerializer;

public class JsonInstance {
	private String note_id;
	private String note_handle;
	private String fv_version;
	private String modality;
	private String feature_vectors;
	private String cpt_codes;
	private String icd_codes;
	public String getIcd_codes() {
		return icd_codes;
	}
	public void setIcd_codes(String icd_codes) {
		this.icd_codes = icd_codes;
	}
	private String is_medicare;
	private String is_merged;
	private double score;
	private String status;
	
	private Date createdDate;
          
	@JsonSerialize(using=DateSerializer.class)
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getNote_id() {
		return note_id;
	}
	public String getNote_handle() {
		return note_handle;
	}
	public String getFv_version() {
		return fv_version;
	}
	public String getModality() {
		return modality;
	}
	public String getCpt_codes() {
		return cpt_codes;
	}
	public void setNote_id(String note_id) {
		this.note_id = note_id;
	}
	public void setNote_handle(String note_handle) {
		this.note_handle = note_handle;
	}
	public void setFv_version(String fv_version) {
		this.fv_version = fv_version;
	}
	public void setModality(String modality) {
		this.modality = modality;
	}
	public void setCpt_codes(String cpt_codes) {
		this.cpt_codes = cpt_codes;
	}
	public String getFeature_vectors() {
		return feature_vectors;
	}
	public String getIs_medicare() {
		return is_medicare;
	}
	public String getIs_merged() {
		return is_merged;
	}
	public void setFeature_vectors(String feature_vectors) {
		this.feature_vectors = feature_vectors;
	}
	public void setIs_medicare(String is_medicare) {
		this.is_medicare = is_medicare;
	}
	public void setIs_merged(String is_merged) {
		this.is_merged = is_merged;
	}
	public double getScore() {
		return score;
	}
	public String getStatus() {
		return status;
	}
	public void setScore(double score) {
		this.score = score;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
}
