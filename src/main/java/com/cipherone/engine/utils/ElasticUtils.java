package com.cipherone.engine.utils;

import io.searchbox.client.JestClient;
import io.searchbox.client.JestClientFactory;
import io.searchbox.client.JestResult;
import io.searchbox.client.config.HttpClientConfig;
import io.searchbox.core.Get;
import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;
import io.searchbox.core.SearchScroll;
import io.searchbox.params.Parameters;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.elasticsearch.action.count.CountResponse;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;

import com.cipherone.engine.commons.Annotation;
import com.cipherone.engine.commons.Document;
import com.cipherone.engine.firebase.FireBaseConnector;
import com.cipherone.engine.svm.DataInstance;
import com.cipherone.engine.svm.ModelEngine;
import com.cipherone.engine.svm.SVMUtility;
import com.cipherone.engine.tools.BackendDaemon;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

public class ElasticUtils {
	private final static String indexName = "documents";
	private final static String documentType = "doc";
	// private final static String fvField = "fv";
	private String hostName = "localhost";
	
	private JestClient jClient;

	public ElasticUtils() {
		initialize();
	}
	
	public ElasticUtils(String _host) {
		hostName = _host;
		initialize();
	}

	private void initialize() {
		JestClientFactory factory = new JestClientFactory();
		factory.setHttpClientConfig(new HttpClientConfig.Builder(hostName).multiThreaded(true).build());
		jClient = factory.getObject();
    }
	
	public void close() {
		if(jClient != null)
			jClient.shutdownClient();
	}
		
	public String getValue(String documentId) throws InterruptedException, ExecutionException, IOException {
		return getTermVector(documentId);
	}
	
	public HashMap<String, Annotation> screening(ModelEngine model, int size) throws IOException {
		return screening(model, false, -0.1, size);
	}

	public HashMap<String, Annotation> screening(ModelEngine model, boolean retrieve, double threshold, int limit) throws IOException {
		System.out.println("Begin..." + (new Date()).toString());
		String topic = model.getHostCode();
		String query;
		if(retrieve) {
			// query = buildQuery();
			SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
	        searchSourceBuilder.query(QueryBuilders.matchAllQuery());
	        query = searchSourceBuilder.toString();
		} else {
			// String query = buildQuery(topic, 0.4, 0.4, 0.6);
			query = buildQuery(topic);			
		}
		
		Search search = new Search.Builder(query)
		                                // multiple index or types can be added.
		                                .addIndex("documents")
		                                .addType("doc")
		                                .setParameter(Parameters.SIZE, BackendDaemon.nDocsForScreening)
		                                .setParameter(Parameters.SCROLL, "5m")
		                                .build();
		
		JestResult result = jClient.execute(search);
		String scrollId = result.getJsonObject().get("_scroll_id").getAsString();
		
		HashMap<String, Annotation> scores = new HashMap<String, Annotation>();
		JsonArray hits = null;
		while(hits == null || hits.size() > 0) {
            SearchScroll scroll = new SearchScroll.Builder(scrollId, "5m")
                    .setParameter(Parameters.SIZE, BackendDaemon.nDocsForScreening).build();
            
            result = jClient.execute(scroll);
            hits = result.getJsonObject().getAsJsonObject("hits").getAsJsonArray("hits");
            
            for (int i=0; i<hits.size(); i++) {
    			Document doc = new Gson().fromJson(hits.get(i).getAsJsonObject().get("_source"), Document.class);
    			DataInstance instance = new DataInstance();
    			instance.setName(doc.getDoc_id());
    			
    			String fvString = doc.getFv();
    			String formatedString = SVMUtility.normalizeFvs(fvString, model.getNormalizationScheme());
    			instance.loadFvs(formatedString);

    			double score = model.predict(instance, threshold);
    			
    			if(score > 0.0) {
    				Annotation annotation = new Annotation();
    				annotation.setId(doc.getDoc_id());
    				annotation.setAnnotation(FireBaseConnector.labelMap.inverse().get("UNK"));
//    				annotation.setScore(score);
//    				annotation.setVerified(false);
    				
    				scores.put(annotation.getId(), annotation);
    			}
            }
            
//            if(! retrieve && scores.size() >= size) {
//            	break;
//            }
            if(scores.size() >= limit) 
            	break;
            
            scrollId = result.getJsonObject().getAsJsonPrimitive("_scroll_id").getAsString();
        }
		
		System.out.println("Finished at " + (new Date()).toString());
		System.out.println("Total hits: " + scores.size());

        return scores;
	}

	public static String buildQuery(String topic, double minScore, double lowerBound, double upperBound) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		
		JsonNode rootNode = mapper.createObjectNode();
		
		// build from bottom to top
		ObjectNode paramNode = mapper.createObjectNode();
		paramNode.put("min_rev", lowerBound);
		paramNode.put("max_rev", upperBound);
		
		ObjectNode scriptNode = mapper.createObjectNode();
		scriptNode.put("lang", "groovy");
		scriptNode.put("file", "filter_score");
		scriptNode.set("params", paramNode);
		
		ObjectNode scriptScoreNode = mapper.createObjectNode();
		scriptScoreNode.set("script", scriptNode);
		
		ObjectNode scriptScoreParent = mapper.createObjectNode();
		scriptScoreParent.set("script_score", scriptScoreNode);
		
		ArrayNode functionNode = mapper.createArrayNode();
		functionNode.add(scriptScoreParent);
		
		ObjectNode matchNode = mapper.createObjectNode();
		ArrayNode fieldsNode = mapper.createArrayNode();
		fieldsNode.insert(0, "title^2");
		fieldsNode.insert(1, "text");
		matchNode.set("fields", fieldsNode);
		matchNode.put("query", topic);
		
		ObjectNode queryNode = mapper.createObjectNode();
		queryNode.set("multi_match", matchNode);
				
		ObjectNode functionScoreNode = mapper.createObjectNode();
		functionScoreNode.set("query", queryNode);
		functionScoreNode.put("boost_mode", "replace");
		functionScoreNode.set("functions", functionNode);
		
		ObjectNode topNode = mapper.createObjectNode();
		topNode.set("function_score", functionScoreNode);
		
		((ObjectNode) rootNode).set("query", topNode);
		((ObjectNode) rootNode).put("min_score", minScore);
		
		String jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(rootNode);
//		System.out.println(jsonString);
		
		return jsonString;
	}

	public static String buildQuery(String topic) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		
		JsonNode rootNode = mapper.createObjectNode();
		
		ObjectNode matchNode = mapper.createObjectNode();
		ArrayNode fieldsNode = mapper.createArrayNode();
		fieldsNode.insert(0, "title^2");
		fieldsNode.insert(1, "text");
		matchNode.set("fields", fieldsNode);
		matchNode.put("query", topic);
		
		ObjectNode queryNode = mapper.createObjectNode();
		queryNode.set("multi_match", matchNode);
				
		((ObjectNode) rootNode).set("query", queryNode);
		
		String jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(rootNode);
//		System.out.println(jsonString);
		
		return jsonString;
	}

	public static String buildQuery() throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		
		JsonNode rootNode = mapper.createObjectNode();
		
		ObjectNode matchNode = mapper.createObjectNode();
//		ArrayNode fieldsNode = mapper.createArrayNode();
//		matchNode.set("fields", fieldsNode);
		
		ObjectNode queryNode = mapper.createObjectNode();
		queryNode.set("match_all", matchNode);
				
		((ObjectNode) rootNode).set("query", queryNode);
		
		String jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(rootNode);
		System.out.println(jsonString);
		
		return jsonString;
	}

	public HashMap<String, Annotation> search(String topic, int size) throws IOException {
		//String query = buildQuery(topic, 0.5, 0.5, 0.6);
		String query = buildQuery(topic);
		
		Search search = new Search.Builder(query)
		                                // multiple index or types can be added.
		                                .addIndex("documents")
		                                .addType("doc")
		                                .setParameter(Parameters.SIZE, size)
		                                .build();

		SearchResult result = jClient.execute(search);
		
		HashMap<String, Annotation> scores = new HashMap<String, Annotation>();
		List<SearchResult.Hit<Document, Void>> hits = result.getHits(Document.class);
		for (SearchResult.Hit<Document, Void> hit : hits) {
			Document doc = hit.source;
			Annotation annotation = new Annotation();
			annotation.setId(doc.getDoc_id());
			annotation.setAnnotation(FireBaseConnector.labelMap.inverse().get("UNK"));
			
			scores.put(annotation.getId(), annotation);
        }

        return scores;
	}

	public String getTermVector(String docId) throws InterruptedException, ExecutionException, IOException {
		Get get = new Get.Builder(indexName, docId).type(documentType).build();

		JestResult result = jClient.execute(get);
		Document doc = result.getSourceAsObject(Document.class);
		return doc.getFv();
	}

	public String getDocument(String docId) throws IOException {
		Get get = new Get.Builder(indexName, docId).type(documentType).build();
	
		JestResult result = jClient.execute(get);
		Document doc = result.getSourceAsObject(Document.class);
		return doc.toString();
	}
}
