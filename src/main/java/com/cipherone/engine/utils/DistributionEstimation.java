package com.cipherone.engine.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.cipherone.engine.svm.DataInstance;
import com.cipherone.engine.svm.ModelEngine;
import com.google.common.primitives.Doubles;
import com.google.common.primitives.Ints;

public class DistributionEstimation {
	double[] params = new double[2];
	
	public DistributionEstimation() {}
	
	public DistributionEstimation(ModelEngine model) {
		params[0] = model.getProbA();
		params[1] = model.getProbB();
	}
	
	public void probabilityEstimate(List<DataInstance> testDocs) {
		ArrayList<Double> scores = new ArrayList<Double>();
		ArrayList<Integer> labels = new ArrayList<Integer>();
		for (DataInstance doc : testDocs) {
			scores.add(doc.getRawScore());
			if(doc.getLabel() == 1)
				labels.add(1);
			else
				labels.add(-1);
		}

		getEstimatedParams(scores, labels);
	}

	public void tranform(List<DataInstance> testNotes) {
		for (DataInstance note : testNotes) {
			note.setConfidence(score2Probability(note.getRawScore(), params[0], params[1]));
		}
	}


	public static double score2Probability(double rawScore, double probA, double probB) {
		double prob = rawScore * probA + probB;
		
		if(prob >= 0.0) {
			double expValue = Math.exp(-1.0 * prob);
			prob = expValue / (1.0 + expValue);
		}
		else
		{
			prob = 1.0 / (1.0 + Math.exp(prob));
		}
		
		return prob;
	}

	public String printParams() {
		return "probA " + Double.toString(params[0]) + " probB " + Double.toString(params[1]);
	}
	
	// this just for sigmoid 
	private void getEstimatedParams(ArrayList<Double> scores, ArrayList<Integer> labels) {
		if(Collections.frequency(labels, 1) > 0 && Collections.frequency(labels, -1) > 0) {
			params = sigmoid_train(scores.size(), Doubles.toArray(scores), Ints.toArray(labels));
		} else {
			params =  null;
		}
	}
	
	// Platt's binary SVM Probablistic Output: an improvement from Lin et al.
	private double[] sigmoid_train(int l, double[] dec_values, int[] labels) {
    	double A, B;
    	double prior1=0, prior0 = 0;

    	for (int i=0; i<l; i++) {
    		if (labels[i] > 0) prior1 += 1;
    		else prior0 += 1;
    	}

		int max_iter=100;       // Maximal number of iterations
		double min_step=1e-10;  // Minimal step taken in line search
		double sigma=1e-12;     // For numerically strict PD of Hessian
		double eps=1e-5;
		double hiTarget=(prior1+1.0)/(prior1+2.0);
		double loTarget=1/(prior0+2.0);
		double[] t= new double[l];
		double fApB,p,q,h11,h22,h21,g1,g2,det,dA,dB,gd,stepsize;
		double newA,newB,newf,d1,d2;

		// Initial Point and Initial Fun Value
		A=0.0; B=Math.log((prior0+1.0)/(prior1+1.0));
		double fval = 0.0;

		for (int i=0; i<l; i++) {
			if (labels[i]>0) t[i]=hiTarget;
			else t[i]=loTarget;
			fApB = dec_values[i]*A+B;
			if (fApB>=0)
				fval += t[i]*fApB + Math.log(1+Math.exp(-fApB));
			else
				fval += (t[i] - 1)*fApB +Math.log(1+Math.exp(fApB));
		}
		
		double[] probAB = new double[2];
		
		for (int iter=0; iter<max_iter; iter++) {
			// Update Gradient and Hessian (use H' = H + sigma I)
			h11=sigma; // numerically ensures strict PD
			h22=sigma;
			h21=0.0;g1=0.0;g2=0.0;
			for (int i=0; i<l; i++) {
				fApB = dec_values[i]*A+B;
				if (fApB >= 0) {
					p=Math.exp(-fApB)/(1.0+Math.exp(-fApB));
					q=1.0/(1.0+Math.exp(-fApB));
				} else {
					p=1.0/(1.0+Math.exp(fApB));
					q=Math.exp(fApB)/(1.0+Math.exp(fApB));
				}
				d2=p*q;
				h11+=dec_values[i]*dec_values[i]*d2;
				h22+=d2;
				h21+=dec_values[i]*d2;
				d1=t[i]-p;
				g1+=dec_values[i]*d1;
				g2+=d1;
			}

			// Stopping Criteria
			if (Math.abs(g1)<eps && Math.abs(g2)<eps)
				break;

			// Finding Newton direction: -inv(H') * g
			det=h11*h22-h21*h21;
			dA=-(h22*g1 - h21 * g2) / det;
			dB=-(-h21*g1+ h11 * g2) / det;
			gd=g1*dA+g2*dB;

			stepsize = 1;           // Line Search
			while (stepsize >= min_step) {
				newA = A + stepsize * dA;
				newB = B + stepsize * dB;

				// New function value
				newf = 0.0;
				for (int i=0; i<l; i++) {
					fApB = dec_values[i]*newA+newB;
					if (fApB >= 0)
						newf += t[i]*fApB + Math.log(1+Math.exp(-fApB));
					else
						newf += (t[i] - 1)*fApB +Math.log(1+Math.exp(fApB));
				}
				// Check sufficient decrease
				if (newf<fval+0.0001*stepsize*gd) {
					A=newA;B=newB;fval=newf;
					break;
				} else {
					stepsize = stepsize / 2.0;
				}
			}

			if (stepsize < min_step) {
				System.out.println("Line search fails in two-class probability estimates");
				break;
			}

			if (iter>=max_iter)
				System.out.println("Reaching maximal iterations in two-class probability estimates");
			probAB[0]=A;probAB[1]=B;
		}
		
		return probAB;
	}

	public double[] getParams() {
		return params;
	}

	public void setParams(double[] params) {
		this.params = params;
	}
}