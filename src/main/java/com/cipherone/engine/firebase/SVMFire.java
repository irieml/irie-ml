/*
 * Firebase SVM Java Library
 *
 * Copyright © 2014 Firebase - All Rights Reserved
 * https://www.firebase.com
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binaryform must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY FIREBASE AS IS AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL FIREBASE BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.cipherone.engine.firebase;

import com.firebase.client.*;
import com.google.common.collect.Maps;

import java.util.*;

/**
 * A SVMFire instance is used to interact data in Firebase.
 */
public class SVMFire {

    /**
     * A listener that can be used to be notified about a successful write or an error on writing.
     */
    public static interface CompletionListener {
        /**
         * Called once a location was successfully saved on the server or an error occurred. On success, the parameter
         * error will be null; in case of an error, the error will be passed to this method.
         * @param key The key whose location was saved
         * @param error The error or null if no error occurred
         */
        public void onComplete(String key, FirebaseError error);
    }

    /**
     * A small wrapper class to forward any events to the LocationEventListener.
     */
    private static class StatusValueEventListener implements ValueEventListener {

        private final StatusCallback callback;

        StatusValueEventListener(StatusCallback callback) {
            this.callback = callback;
        }

        public void onDataChange(DataSnapshot dataSnapshot) {
            if (dataSnapshot.getValue() == null) {
                this.callback.onStatusResult(dataSnapshot.getKey(), null);
            } else {
                HashMap<String, String> status = SVMFire.getStatusValue(dataSnapshot);
                if (status != null) {
                    this.callback.onStatusResult(dataSnapshot.getKey(), status);
                } else {
                    String message = "GeoFire data has invalid format: " + dataSnapshot.getValue();
                    this.callback.onCancelled(new FirebaseError(FirebaseError.UNKNOWN_ERROR, message));
                }
            }
        }

        public void onCancelled(FirebaseError firebaseError) {
            this.callback.onCancelled(firebaseError);
        }
    }

    static HashMap<String, String> getStatusValue(DataSnapshot dataSnapshot) {
        try {
        	HashMap<String, String> data = Maps.newHashMap(dataSnapshot.getValue(Map.class));
            return data;
        } catch (FirebaseException e) {
            return null;
        } catch (NullPointerException e) {
            return null;
        } catch (ClassCastException e) {
            return null;
        }
    }

    private final Firebase firebase;
    private final StatusCallback callback = null;

    /**
     * Creates a new GeoFire instance at the given Firebase reference.
     * @param firebase The Firebase reference this GeoFire instance uses
     */
    public SVMFire(Firebase firebase) {
        this.firebase = firebase;
    }

    /**
     * @return The Firebase reference this GeoFire instance uses
     */
    public Firebase getFirebase() {
        return this.firebase;
    }

    Firebase firebaseRefForKey(String key) {
        return this.firebase.child(key);
    }

    /**
     * Sets the location for a given key.
     * @param key The key to save the location for
     * @param location The location of this key
     */
    public void setStatus(String key, String status) {
        this.setStatus(key, status, null);
    }

    /**
     * Sets the location for a given key.
     * @param key The key to save the location for
     * @param location The location of this key
     * @param completionListener A listener that is called once the location was successfully saved on the server or an
     *                           error occurred
     */
    public void setStatus(final String key, final String status, final CompletionListener completionListener) {
        if (key == null) {
            throw new NullPointerException();
        }
        Firebase keyRef = this.firebaseRefForKey(key);
        if (completionListener != null) {
            keyRef.setValue(status, new Firebase.CompletionListener() {
                public void onComplete(FirebaseError error, Firebase firebase) {
                    if (completionListener != null) {
                        completionListener.onComplete(key, error);
                    }
                }
            });
        } else {
            keyRef.setValue(status);
        }
    }

    /**
     * Removes the location for a key from this GeoFire.
     * @param key The key to remove from this GeoFire
     */
    public void removeStatus(String key) {
        this.removeStatus(key, null);
    }

    /**
     * Removes the location for a key from this GeoFire.
     * @param key The key to remove from this GeoFire
     * @param completionListener A completion listener that is called once the location is successfully removed
     *                           from the server or an error occurred
     */
    public void removeStatus(final String key, final CompletionListener completionListener) {
        if (key == null) {
            throw new NullPointerException();
        }
        Firebase keyRef = this.firebaseRefForKey(key);
        if (completionListener != null) {
            keyRef.setValue(null, new Firebase.CompletionListener() {
                public void onComplete(FirebaseError error, Firebase firebase) {
                    completionListener.onComplete(key, error);
                }
            });
        } else {
            keyRef.setValue(null);
        }
    }

    /**
     * Gets the current location for a key and calls the callback with the current value.
     *
     * @param key The key whose location to get
     * @param callback The callback that is called once the location is retrieved
     */
    public void getStatus(String key) {
        Firebase keyFirebase = this.firebaseRefForKey(key);
        StatusValueEventListener valueListener = new StatusValueEventListener(this.callback);
        keyFirebase.addListenerForSingleValueEvent(valueListener);
    }

    /**
     * Returns a new Query object centered at the given location and with the given radius.
     * @param center The center of the query
     * @param radius The radius of the query, in kilometers
     * @return The new GeoQuery object
     */
    public StatusQuery queryAtTopic(String status, int iteration) {
        return new StatusQuery(this, status, iteration);
    }
}
