package com.cipherone.engine.firebase;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.cipherone.engine.commons.Annotation;
import com.cipherone.engine.commons.PerformanceScore;
import com.cipherone.engine.svm.DataInstance;
import com.cipherone.engine.svm.DataSet;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

public class FireBaseConnector {
	private Firebase dataRef;
	private String url = "https://iriedata.firebaseio.com";
	private Map<String, String> reviewStatus = new HashMap<String, String>();
	private HashMap<String, DataSet> reviewedDocuments = new HashMap<String, DataSet>();
	
	public final static String LEARN = "Learning";
	public final static String REVIEW = "Review";
	public final static String LOCKED = "Processing";
	public final static String RETRIEVE = "Retrieving";
	public final static String DONE = "Saved";
	public final static HashBiMap<String, String> labelMap;
	static {
		HashBiMap<String, String> aMap =HashBiMap.create();
		aMap.put("+", "GOOD");
		aMap.put("-", "BAD");
		aMap.put("?", "UNK");
		labelMap = HashBiMap.create(aMap);
	}
	
	/*
	 * TopicName: "real name"
	 * TopicStatus: "Learning", "ReadyForML", "ReadyForReview"
	 */

	public FireBaseConnector() {
		dataRef = new Firebase(url);
	}
	
	public enum WorkStageStatus {
		ReadyForML(1), LockForML(2), ReadyForReview(3), LockForReview(4), Complete(10); 
		private int value; private WorkStageStatus(int value) { this.value = value; } 
	}; 

	// Read more: http://javarevisited.blogspot.com/2011/08/enum-in-java-example-tutorial.html#ixzz3xQCRvcFj

	public HashMap<String, DataSet> getReviewedDocuments() {
		return reviewedDocuments;
	}

	public FireBaseConnector(String _url) {
		url = _url;
		dataRef = new Firebase(url);
	}

	public void addRecord(String topic, Annotation annotation) {
		String endPoint = "topics/" + topic + "/Documents";
		// dataRef.child(endPoint).child(annotation.getId()).push().setValue(annotation);
		dataRef.child(endPoint).child(annotation.getId()).setValue(annotation);
	}

	public void addTopic(String topic, String desc) {
		String endPoint = "topics/" + topic;
		dataRef.child(endPoint).child("TopicStatus").setValue("Initialized");
		dataRef.child(endPoint).child("TopicName").setValue(desc);
	}

	public void updateStatus(String topic, String status) {
		// String endPoint = "ReviewStatus/" + topic;
		String endPoint = "topics/" + topic + "/TopicStatus";
		dataRef.child(endPoint).setValue(status);
		reviewStatus.put(topic, status);
	}
	
	public void extractStatus0() {
		String endPoint = "ReviewStatus";
		dataRef.child(endPoint).addValueEventListener(new ValueEventListener() {
		       public void onDataChange(DataSnapshot snapshot) {
		    	   reviewStatus = (Map<String, String>) snapshot.getValue(Map.class);
		           for (String topic : reviewStatus.keySet()) {
		             System.out.println(topic + " - " + reviewStatus.get(topic));
		           }
		       }
		       public void onCancelled(FirebaseError firebaseError) {
		           System.out.println("The read failed: " + firebaseError.getMessage());
		       }
		   });
	}

	public void setStatusUpdate() {
		String endPoint = "topics";
		dataRef.child(endPoint).addValueEventListener(new ValueEventListener() {
		       public void onDataChange(DataSnapshot snapshot) {
		    	   for (DataSnapshot child : snapshot.getChildren()) {
		    		   DataSnapshot statusObj = child.child("TopicStatus");
		    		   String status = (String) statusObj.getValue();
		    		   reviewStatus.put(child.getKey(), status);
		    	   }
		       }

		       public void onCancelled(FirebaseError firebaseError) {
		           System.out.println("The read failed: " + firebaseError.getMessage());
		       }
		   });
	}

	public void extractStatus() {
		String endPoint = "topics";
		dataRef.child(endPoint).addListenerForSingleValueEvent(new ValueEventListener() {
		       public void onDataChange(DataSnapshot snapshot) {
		    	   for (DataSnapshot child : snapshot.getChildren()) {
		    		   DataSnapshot statusObj = child.child("TopicStatus");
		    		   String status = (String) statusObj.getValue();
		    		   reviewStatus.put(child.getKey(), status);
		    	   }
		       }

		       public void onCancelled(FirebaseError firebaseError) {
		           System.out.println("The read failed: " + firebaseError.getMessage());
		       }
		   });
	}

	public void extractAnnotations(final String topic) {
		String endPoint = "topics/" + topic + "/Documents";
		dataRef.child(endPoint).addListenerForSingleValueEvent(new ValueEventListener() {
		       public void onDataChange(DataSnapshot snapshot) {
		    	   List<DataInstance> instances = Lists.newArrayList();
//		    	   List<Annotation> annotations = (List<Annotation>) snapshot.getValue();
//		    	   for (Annotation annotation : annotations) {
//		    		   String classCode = FireBaseConnector.labelMap.get(annotation.getAnnotation());
//		    		   if(! classCode.equalsIgnoreCase("UNK")) {
//			    		   DataInstance instance = new DataInstance();
//			    		   instance.setName(annotation.getId());
//			    		   instance.setAnnotation(Sets.newHashSet(classCode));
//			    		   instances.add(instance);
//		    		   }
//		    	   }
		    	   Set<String> uniqueIds = Sets.newHashSet();
		    	   for (DataSnapshot annotation : snapshot.getChildren()) {
		    		   String docId;
		    		   String label;
		    		   if(annotation.hasChild("Id")) {
		    			   docId = (String) annotation.child("Id").getValue();
		    			   label = (String) annotation.child("Annotation").getValue();
		    		   } else {
		    			   docId = (String) annotation.child("id").getValue();
		    			   label = (String) annotation.child("annotation").getValue();
		    		   }

		    		   String classCode = FireBaseConnector.labelMap.get(label); // should be GOOD, BAD, or UNK
		    		   int instId = Integer.parseInt(annotation.getKey());
		    		   if(! classCode.equalsIgnoreCase("UNK") && !uniqueIds.contains(docId)) {
		    			   DataInstance instance = new DataInstance();
		    			   instance.setId(instId);
		    			   instance.setName(docId);
		    			   instance.setAnnotation(Sets.newHashSet(classCode));
		    			   instances.add(instance);
		    			   uniqueIds.add(docId);
		    			   
		    		   } else {
		    			   annotation.getRef().removeValue();
		    		   }
		    	   }
		    	   System.out.println(topic + " train size: " + instances.size());
		    	   reviewedDocuments.put(topic, new DataSet(instances));
		       }
		       
		       public void onCancelled(FirebaseError firebaseError) {
		           System.out.println("The read failed: " + firebaseError.getMessage());
		       }
		   });
	}

	public String getStatus(String topic) {
		if(reviewStatus.containsKey(topic))
			return reviewStatus.get(topic);
		else
			return null;
	}

	public void updateAnnotation(String topic, String id, String label) {
		String endPoint = "topics/" + topic + "/Documents";
		dataRef.child(endPoint).child(id).child("Annotation").setValue(label);
	}

	public void updateAnnotation(String topic, int index, Annotation annotation) {
		String endPoint = "topics/" + topic + "/Documents";
		dataRef.child(endPoint).child(Integer.toString(index)).child("Id").setValue(annotation.getId());
		dataRef.child(endPoint).child(Integer.toString(index)).child("Annotation").setValue(annotation.getAnnotation());
//		dataRef.child(endPoint).child(Integer.toString(index)).setValue(annotation); // .child("Annotation").setValue(annotation.getAnnotation());
	}

	public void updateAnnotation(String topic, List<Annotation> annotations) {
		String endPoint = "topics/" + topic + "/Documents";
		dataRef.child(endPoint).setValue(annotations);
	}

	public void updatePerformanceScore(String topic, PerformanceScore score) {
		String endPoint = "topics/" + topic + "/PerformanceExpectation";
//		dataRef.child(endPoint).setValue(score);
		dataRef.child(endPoint).child("precision").setValue(String.format("%.2f", score.getPrecision()));
		dataRef.child(endPoint).child("recall").setValue(String.format("%.2f", score.getRecall()));
		dataRef.child(endPoint).child("f-score").setValue(String.format("%.2f", score.getfScore()));
	}

	public void addMetric(String topic, PerformanceScore scores) {
		String endPoint = "topics/" + topic + "/Performance";
		dataRef.child(endPoint).push().setValue(scores);
	}
	
	public List<Annotation> getAnnotations() {
		List<Annotation> annotations = Lists.newArrayList();
		return annotations;
	}
	
	public void showRecords() {
		dataRef.addListenerForSingleValueEvent(new ValueEventListener() {
		    public void onDataChange(DataSnapshot snapshot) {
		        for (DataSnapshot child : snapshot.getChildren()) {
		        	url = "";
		        	System.out.println(child.getValue().toString());
		        }
		    }
		       public void onCancelled(FirebaseError firebaseError) {
	           System.out.println("The read failed: " + firebaseError.getMessage());
	       }
		});
	}
	
	public void close() {
		Firebase.goOffline();
	}

	public Map<String, String> getReviewStatus() {
		return reviewStatus;
	}
	
}
