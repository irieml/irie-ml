/*
 * Firebase GeoFire Java Library
 *
 * Copyright © 2014 Firebase - All Rights Reserved
 * https://www.firebase.com
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binaryform must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY FIREBASE AS IS AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL FIREBASE BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.cipherone.engine.firebase;

import com.firebase.client.*;

import java.util.*;

/**
 * A GeoQuery object can be used for geo queries in a given circle. The GeoQuery class is thread safe.
 */
public class StatusQuery {

    private static class TopicInfo {
        String status = "";
        int iteration = 1;
        String desc;
        String id;
        boolean inSvmQuery = false;

        public TopicInfo(String status, int interation) {
            this.status = status;
            this.iteration = interation;
        }
    }

    private final ChildEventListener childEventLister = new ChildEventListener() {
        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
            synchronized (StatusQuery.this) {
                StatusQuery.this.childAdded(dataSnapshot);
            }
        }

        public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            synchronized (StatusQuery.this) {
                StatusQuery.this.childChanged(dataSnapshot);
            }
        }

        public void onChildRemoved(DataSnapshot dataSnapshot) {
            synchronized (StatusQuery.this) {
                StatusQuery.this.childRemoved(dataSnapshot);
            }
        }

        public synchronized void onChildMoved(DataSnapshot dataSnapshot, String s) {
            // ignore, this should be handled by onChildChanged
        }

        public synchronized void onCancelled(FirebaseError firebaseError) {
            // ignore, our API does not support onCancelled
        }
    };

    private final SVMFire svmFire;
    private final Set<SvmQueryEventListener> eventListeners = new HashSet<SvmQueryEventListener>();
    private final Map<StatusQuery, Query> firebaseQueries = new HashMap<StatusQuery, Query>();
    private final Set<StatusQuery> outstandingQueries = new HashSet<StatusQuery>();
    private final Map<String, TopicInfo> topicInfos = new HashMap<String, TopicInfo>();
    private String status;
    private int iteration;
    private Set<StatusQuery> queries;

    /**
     * Creates a new GeoQuery object centered at the given location and with the given radius.
     * @param geoFire The GeoFire object this GeoQuery uses
     * @param center The center of this query
     * @param radius The radius of this query, in kilometers
     */
    StatusQuery(SVMFire svmFire, String status, int iteration) {
        this.svmFire = svmFire;
        this.status = status;
        this.iteration = iteration;
    }

    private boolean statusIsInQuery(String status) {
        return status.equalsIgnoreCase("ReadyForML");
    }

    private void postEvent(Runnable r) {
        EventTarget target = Firebase.getDefaultConfig().getEventTarget();
        target.postEvent(r);
    }

    private void updateTopicInfo(final String key, final TopicInfo topicInfo) {
    	String status = topicInfo.status;
    	TopicInfo oldInfo = this.topicInfos.get(key);
        boolean isNew = (oldInfo == null);
        boolean changedStatus = (oldInfo != null && !oldInfo.status.equalsIgnoreCase(status));
        boolean wasInQuery = (oldInfo != null && oldInfo.inSvmQuery);

        boolean isInQuery = this.statusIsInQuery(status);
        if ((isNew || !wasInQuery) && isInQuery) {
            for (final SvmQueryEventListener listener: this.eventListeners) {
                postEvent(new Runnable() {
                    public void run() {
                        // listener.onKeyEntered(key, location);
                    	// someting trigger the svm training
                    }
                });
            }
        } else if (!isNew && changedStatus && isInQuery) {
            for (final SvmQueryEventListener listener: this.eventListeners) {
                postEvent(new Runnable() {
                    public void run() {
                        // listener.onKeyMoved(key, location);
                    }
                });
            }
        } else if (wasInQuery && !isInQuery) {
            for (final SvmQueryEventListener listener: this.eventListeners) {
                postEvent(new Runnable() {
                    public void run() {
                        // listener.onKeyExited(key);
                    }
                });
            }
        }
        TopicInfo newInfo = new TopicInfo(status, topicInfo.iteration + 1);
        this.topicInfos.put(key, newInfo);

    }
    private void updateTopicInfo(final String key, final HashMap<String, String> topicStatus) {
    	for (String topicId : topicStatus.keySet()) {
    		this.updateTopicInfo(key, topicInfos.get(topicId));
    	}
    }

    private void reset() {
        this.topicInfos.clear();
    }

    private boolean hasListeners() {
        return !this.eventListeners.isEmpty();
    }

    private boolean canFireReady() {
        return this.outstandingQueries.isEmpty();
    }

    private void checkAndFireReady() {
        if (canFireReady()) {
            for (final SvmQueryEventListener listener: this.eventListeners) {
                postEvent(new Runnable() {
                    public void run() {
                        listener.onSvmQueryReady();
                    }
                });
            }
        }
    }

    private void addValueToReadyListener(final Query firebase, final String query) {
        firebase.addListenerForSingleValueEvent(new ValueEventListener() {
            public void onDataChange(DataSnapshot dataSnapshot) {
                synchronized (StatusQuery.this) {
                    StatusQuery.this.outstandingQueries.remove(query);
                    StatusQuery.this.checkAndFireReady();
                }
            }

            public void onCancelled(final FirebaseError firebaseError) {
                synchronized (StatusQuery.this) {
                    for (final SvmQueryEventListener listener : StatusQuery.this.eventListeners) {
                        postEvent(new Runnable() {
                            public void run() {
                                // listener.onGeoQueryError(firebaseError);
                            }
                        });
                    }
                }
            }
        });
    }

    private void setupQueries() {
//        Set<GeoHashQuery> oldQueries = (this.queries == null) ? new HashSet<GeoHashQuery>() : this.queries;
//        Set<GeoHashQuery> newQueries = GeoHashQuery.queriesAtLocation(center, radius);
//        this.queries = newQueries;
//        for (GeoHashQuery query: oldQueries) {
//            if (!newQueries.contains(query)) {
//                firebaseQueries.get(query).removeEventListener(this.childEventLister);
//                firebaseQueries.remove(query);
//                outstandingQueries.remove(query);
//            }
//        }
//        for (final GeoHashQuery query: newQueries) {
//            if (!oldQueries.contains(query)) {
//                outstandingQueries.add(query);
//                Firebase firebase = this.geoFire.getFirebase();
//                Query firebaseQuery = firebase.orderByChild("g").startAt(query.getStartValue()).endAt(query.getEndValue());
//                firebaseQuery.addChildEventListener(this.childEventLister);
//                addValueToReadyListener(firebaseQuery, query);
//                firebaseQueries.put(query, firebaseQuery);
//            }
//        }
//        for(Map.Entry<String, LocationInfo> info: this.locationInfos.entrySet()) {
//            LocationInfo oldLocationInfo = info.getValue();
//            this.updateLocationInfo(info.getKey(), oldLocationInfo.location);
//        }
//        // remove locations that are not part of the geo query anymore
//        Iterator<Map.Entry<String, LocationInfo>> it = this.locationInfos.entrySet().iterator();
//        while (it.hasNext()) {
//            Map.Entry<String, LocationInfo> entry = it.next();
//            if (!this.geoHashQueriesContainGeoHash(entry.getValue().geoHash)) {
//                it.remove();
//            }
//        }

        checkAndFireReady();
    }

    private void childAdded(DataSnapshot dataSnapshot) {
        HashMap<String, String> topicStatus = SVMFire.getStatusValue(dataSnapshot);
        if (topicStatus != null) {
            this.updateTopicInfo(dataSnapshot.getKey(), topicStatus);
        } else {
            // throw an error in future?
        }
    }

    private void childChanged(DataSnapshot dataSnapshot) {
    	HashMap<String, String> topicStatus = SVMFire.getStatusValue(dataSnapshot);
        if (topicStatus != null) {
            this.updateTopicInfo(dataSnapshot.getKey(), topicStatus);
        } else {
            // throw an error in future?
        }
    }

    private void childRemoved(DataSnapshot dataSnapshot) {
        final String key = dataSnapshot.getKey();
        final TopicInfo info = this.topicInfos.get(key);
        if (info != null) {
            this.svmFire.firebaseRefForKey(key).addListenerForSingleValueEvent(new ValueEventListener() {
                public void onDataChange(DataSnapshot dataSnapshot) {
                    synchronized(StatusQuery.this) {
                        HashMap<String, String> status = SVMFire.getStatusValue(dataSnapshot);
                        for (String topicId : status.keySet()) {
                            final TopicInfo info = StatusQuery.this.topicInfos.get(topicId);
                            StatusQuery.this.topicInfos.remove(key);
                            if (info != null && info.inSvmQuery) {
                                for (final SvmQueryEventListener listener: StatusQuery.this.eventListeners) {
                                    postEvent(new Runnable() {
                                        public void run() {
                                            // listener.onKeyExited(key);
                                        }
                                    });
                                }
                            }
                        }
                    }
                }

                public void onCancelled(FirebaseError firebaseError) {
                    // tough luck
                }
            });
        }
    }

    /**
     * Adds a new GeoQueryEventListener to this GeoQuery.
     *
     * @throws java.lang.IllegalArgumentException If this listener was already added
     *
     * @param listener The listener to add
     */
    public synchronized void addSvmQueryEventListener(final SvmQueryEventListener listener) {
        if (eventListeners.contains(listener)) {
            throw new IllegalArgumentException("Added the same listener twice to a GeoQuery!");
        }
        eventListeners.add(listener);
        if (this.queries == null) {
            this.setupQueries();
        } else {
            for (final Map.Entry<String, TopicInfo> entry: this.topicInfos.entrySet()) {
                final String key = entry.getKey();
                final TopicInfo info = entry.getValue();
                if (info.inSvmQuery) {
                    postEvent(new Runnable() {
                        public void run() {
                            //listener.onKeyEntered(key, info.location);
                        }
                    });
                }
            }
            if (this.canFireReady()) {
                postEvent(new Runnable() {
                    public void run() {
                        // listener.onGeoQueryReady();
                    }
                });
            }
        }
    }

    /**
     * Removes an event listener.
     *
     * @throws java.lang.IllegalArgumentException If the listener was removed already or never added
     *
     * @param listener The listener to remove
     */
    public synchronized void removeSvmQueryEventListener(SvmQueryEventListener listener) {
        if (!eventListeners.contains(listener)) {
            throw new IllegalArgumentException("Trying to remove listener that was removed or not added!");
        }
        eventListeners.remove(listener);
        if (!this.hasListeners()) {
            reset();
        }
    }

    /**
     * Removes all event listeners from this StatusQuery.
     */
    public synchronized void removeAllListeners() {
        eventListeners.clear();
        reset();
    }

    public synchronized int getIteration() {
        return iteration;
    }

    public synchronized void setIteration(int interation) {
        this.iteration = iteration;
        if (this.hasListeners()) {
            this.setupQueries();
        }
    }

    public synchronized String getStatus() {
        return status;
    }

    public synchronized void setStatus(String status) {
    	this.status = status;
        if (this.hasListeners()) {
            this.setupQueries();
        }
    }
}
