package com.cipherone.engine.firebase;

import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

public class FireBaseDeamon {

    public static void main(String[] args) throws InterruptedException {
        Firebase firebase = new Firebase("https://geofire-v3.firebaseio.com/geofire");
        SVMFire svmFire = new SVMFire(firebase);
        StatusQuery query = svmFire.queryAtTopic("", 0);
        query.addSvmQueryEventListener(new SvmQueryEventListener() {
            public void onSvmQueryReady() {
                System.out.println("All initial key entered events have been fired!");
            }

            public void onSvmQueryError(FirebaseError error) {
                System.err.println("There was an error querying locations: " + error.getMessage());
            }
        });
        // run for another 60 seconds
        Thread.sleep(60000);
    }
}
