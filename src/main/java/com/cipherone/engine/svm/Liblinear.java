package com.cipherone.engine.svm;

import java.util.HashMap;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

// import org.apache.log4j.Logger;


import com.google.common.collect.HashBiMap;

import de.bwaldvogel.liblinear.FeatureNode;
import de.bwaldvogel.liblinear.Linear;
import de.bwaldvogel.liblinear.Model;
import de.bwaldvogel.liblinear.Parameter;
import de.bwaldvogel.liblinear.Problem;
import de.bwaldvogel.liblinear.SolverType;

public class Liblinear {
	// static Logger log = Logger.getLogger(Liblinear.class);
	
	final static double eps = 0.01; // stopping criteria
	public static SolverType solver = SolverType.L2R_L2LOSS_SVC_DUAL; // -s 1 is default, could be -s 5 for L1L2
	HashBiMap<String, Integer> indexmap;
	

    public HashBiMap<String, Integer> getIndexmap() {
		return indexmap;
	}

	public void setIndexmap(HashBiMap<String, Integer> indexmap) {
		this.indexmap = indexmap;
	}

	private Problem formProblem(Iterable<DataInstance> dataset) {
    	return formProblem(dataset, false);
    }
    
    private Problem formProblem(Iterable<DataInstance> dataset, boolean applyWeight) {
    	int problemSize = 0;
    	for (DataInstance instance: dataset) {
    		int repeats = 1;
    		if(applyWeight) repeats = (int) instance.getWeight();
    		problemSize += repeats;
    	}

    	Problem problem = new Problem();
    	problem.l = problemSize;
    	problem.x = new FeatureNode[problem.l][]; // feature nodes
    	problem.y = new double[problem.l];
    	int i = 0;
    	int nFeatures = 0;
    	for (DataInstance instance: dataset) {
    		int repeats = 1;
    		if(applyWeight) repeats = (int) instance.getWeight();
	    	HashMap<Integer, Double> fvs = formSVMVector(instance);
     		SortedSet<Integer> featureIndexes = new TreeSet<Integer>(fvs.keySet());
	    	if(nFeatures < featureIndexes.last()) nFeatures = featureIndexes.last();
     		for (int k=0; k<repeats; k++) {
	    		problem.y[i] = instance.getLabel();
	    		problem.x[i] = new FeatureNode[fvs.size()];
	    		int j = 0;
	    		for (int featureIndex : featureIndexes) {
	    			problem.x[i][j] = new FeatureNode(featureIndex, fvs.get(featureIndex));
	    			j ++;
	    		}
	    		
	    		if (++i == problemSize)
	    			break;
    		}
    	}
    	problem.n = nFeatures;  // Note -- problem.n set here, as featureMap size grows during iteration through data instances above.
    	return problem;
    }
    
    private HashMap<Integer, Double> formSVMVector(DataInstance instance) {
    	HashMap<Integer, Double> fvs = new HashMap<Integer, Double>(instance.getFvs().size());
    	
    	for (Map.Entry<String, Double> entry : instance.getFvs().entrySet()) {
    		if(indexmap.containsKey(entry.getKey())) {
    			fvs.put(indexmap.get(entry.getKey()), entry.getValue());
    		}
    	}
    	return fvs;
    }

    public Model train(PositiveNegativeInstances pni, OVA_Parameters params, String targetCode) {
 		// log.debug("Params.modelID: " + params.getModelId());
 		Problem problem;
 		if(! params.applyInstanceWeight)
 			problem = formProblem(pni);
 		else
 			problem = formProblem(pni, true);
 	
		double C = 1.0;
		if(solver.getId() == 1) {
	    	PositiveNegativeInstances trainSubset = DataSet.drawSubset(targetCode, params.sampleSize, params.sampleSize, pni);
			SVMParameters svmp = new SVMParameters(params.robustnessCut, params.p2nScale, trainSubset);
			C = svmp.getCValue();
		}
		// System.out.println("SVMP.C: " + C + " solver " + solver.getId());
	   	Parameter parameter = new Parameter(solver, C, eps);

    	int[] weightLabels = new int[2];
    	// There is a way to control precision/recall through weights setting
    	// For large exemplars, may not apply following scale scheme
    	// For smaller exemplars, the scale can be reduced to a fixed number, say 2, 5, 10?
    	// Or progressively increase or reduce the scale, or select the best scale for each code?
    	double[] weights = new double[2];
    	weightLabels[0] = 1;
   		weights[0] =  (double)pni.numberOfNegativeNotes() / (double)pni.numberOfPositiveNotes(); // params.getP2nScale();

    	weightLabels[1] = -1;
    	weights[1] = 1.0;
    	
    	parameter.setWeights(weights, weightLabels);

    	//log.debug("Training... " + problem + "...." + targetCode);
    	// Stopwatch sw = Stopwatch.createStarted();
    	
    	Linear.disableDebugOutput();
    	
    	Model model = Linear.train(problem, parameter);
    	// log.debug("Done training... " + problem + "...." + targetCode + " in " + sw.elapsed(TimeUnit.SECONDS) + " seconds");
    	return model;
    }
}
