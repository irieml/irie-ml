package com.cipherone.engine.svm;

import java.util.Date;

import com.cipherone.engine.commons.PerformanceScore;

public class ModelInfo {
	private Date created;
	private PerformanceScore performance;
	private int numNotesUsed;
	private int numNotesTested;
	private String version;

	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public int getNumNotesUsed() {
		return numNotesUsed;
	}
	public void setNumNotesUsed(int numNotesUsed) {
		this.numNotesUsed = numNotesUsed;
	}
	public int getNumNotesTested() {
		return numNotesTested;
	}
	public void setNumNotesTested(int numNotesTested) {
		this.numNotesTested = numNotesTested;
	}
	public PerformanceScore getPerformance() {
		return performance;
	}
	public void setPerformance(PerformanceScore performance) {
		this.performance = performance;
	}
}
