package com.cipherone.engine.svm;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.GZIPInputStream;

import com.cipherone.engine.utils.DistributionEstimation;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class SVMModel {
	final private ModelEngine engine;
	final private ModelInfo info;

	public SVMModel(ModelEngine engine, ModelInfo info) {
		this.engine = engine;
		this.info = info;
	}
	
	public SVMModel(String modelFile) throws FileNotFoundException, IOException {
		InputStream in = null;
		if(modelFile.endsWith(".gz")) {
			in = new GZIPInputStream(new FileInputStream(modelFile));
		} else {
			System.out.println("Loading model filename:" + modelFile);
			in = new FileInputStream(modelFile);
		}
		BufferedReader br = new BufferedReader(new InputStreamReader(in, "UTF-8"));
		
		ObjectMapper mapper = new ObjectMapper();
		JsonNode rootNode = mapper.readTree(br);
		br.close();

		JsonNode engineNode = rootNode.path("engine");
		engine = mapper.treeToValue(engineNode, ModelEngine.class);

		JsonNode infoNode = rootNode.path("meta");
		info = mapper.treeToValue(infoNode, ModelInfo.class);
	}
	
	public String toString() {
		try {
			return toJson();
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	public String toJson() throws JsonGenerationException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		
		JsonNode rootNode = mapper.createObjectNode();
		
		((ObjectNode) rootNode).set("engine", mapper.valueToTree(engine));
		((ObjectNode) rootNode).set("meta", mapper.valueToTree(info));
		
		return mapper.writeValueAsString(rootNode);
		
		// mapper.writeValue(new File(engine.getHostCode() + ".json"), rootNode);
	}
	
	public void saveModel(String modelFile) throws IOException {
		FileWriter outModel = new FileWriter(modelFile);
		
		outModel.write(toString());
		outModel.write('\n');
		outModel.flush();
		outModel.close();
	}
	
	public static void saveModel(String content, String modelFile) throws IOException {
		FileWriter outModel = new FileWriter(modelFile);
		outModel.write(content);
		outModel.write('\n');
		outModel.flush();
		outModel.close();
	}

	/**
	 * predict
	 * 
	 * iterates through the dataset and calculates the score of each instance within the dataset
	 * 
	 * @param dataset
	 * @param cutoff
	 * @param setProb
	 * @return
	 */
    public HashMap<String, Double> predict(DataSet dataset, double cutoff) {
    	HashMap<String, Double> scores = new HashMap<String, Double>(dataset.instances.size());
    	// String[] fields = info.getVersion().split("\\.");
    	String[] fields = engine.getVersion().split("\\.");
    	for (int i=0; i<dataset.instances.size(); i++) {
    		DataInstance instance = dataset.getDataInstance(i);
    		// may need more specific comparison
    		String[] fvVersion = instance.getVersion().split("\\.");
    		// evaluate any fvs which have the same fv major version
    		if(fields[1].equals(fvVersion[0])) {
	    		double score = predict(instance, cutoff);
	    		if(score >= cutoff)
	    			scores.put(instance.getName(), score);
    		}
    	}
    	return scores;
     }
    
    public double predict(DataInstance instance, double cutoff) {
		double score = computeRawScore(instance.getFvs());
		if(score < cutoff) return (double)-999.0; 
		return score;
	}

	public double predict(HashMap<String, Double> testFvs) {
		double score = computeRawScore(testFvs);
		return score;
	}

	public double computeRawScore(HashMap<String, Double> testFvs) {
		double score = 0.0;
		HashMap<String, Double> fvs = engine.getFvs();
		String normalizationScheme = engine.getNormalizationScheme();
		for (Map.Entry<String, Double> entry : testFvs.entrySet()) {
			String feature = entry.getKey();
			if(fvs.containsKey(feature)) {
				double weight = fvs.get(feature);
				if(normalizationScheme == null || ! normalizationScheme.equalsIgnoreCase("binary"))
					weight *= entry.getValue();
				score += weight;
			}
		}

		score -= engine.getRho();
		score *= engine.getSign();

		if(! engine.getModelType().equalsIgnoreCase("OVA")) {
			score = DistributionEstimation.score2Probability(score, engine.getProbA(), engine.getProbB());
		}

		return (double) score;
	}

	public ModelEngine getEngine() {
		return engine;
	}

	public ModelInfo getInfo() {
		return info;
	}
}
