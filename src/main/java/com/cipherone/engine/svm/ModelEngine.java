package com.cipherone.engine.svm;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.cipherone.engine.utils.DistributionEstimation;
import com.google.common.collect.BiMap;

import de.bwaldvogel.liblinear.Model;

public class ModelEngine {
	final private int sign;
	final private String version;
	final private double rho;
	final private double probA;
	final private double probB;
	final double minimumWeight = (double) 0.001;
	final public HashMap<String, Double> fvs; //only mutated in constructor
	final String normalizationScheme;
	final String hostCode;
	final String modelType;
		
	public ModelEngine(String modelLine) {
		String[] fields = modelLine.split("\\s+");
		
		hostCode = fields[0];
		modelType = fields[1];
		sign = Integer.parseInt(fields[2]);
		rho = Double.parseDouble(fields[3]);
		probA = Double.parseDouble(fields[4]);
		probB = Double.parseDouble(fields[5]);
		if(fields[6].equals("0.0"))
			version = "1.0.0";
		else
			version = fields[6];
		normalizationScheme = fields[7];
	
		fvs = new HashMap<String, Double>(fields.length);
		for (int i=8; i<fields.length; i++) {
			int pos = fields[i].indexOf(':');
			String feature = fields[i].substring(0, pos);
			double weight = Double.parseDouble(fields[i].substring(pos + 1));
			if(Math.abs(weight) >= minimumWeight) {
				fvs.put(feature, weight);
			}
		}
	}

	public ModelEngine(Model model, String id, String normf, String mType, String _version, BiMap<Integer, String> index2Features) {
		int n = model.getNrFeature();
		if(model.getBias() >= 0) n ++;
		sign = model.getLabels()[0];
		rho = (double) model.getBias(); // check, it should be always 0.0
		probA = (double) -1.0;
		probB = (double) 1.0;
		if(_version.equals("0.0"))
			version = "1.0.0";
		else
			version = _version;

		normalizationScheme = normf;
		modelType = mType;
		hostCode = id;
				
		double[] weights = model.getFeatureWeights();
		if(n != weights.length) {
			System.out.println(hostCode + " has model with n=" + n + " but weight length=" + weights.length);
		}
		
		fvs = new HashMap<String, Double>(n);
		for (int i=0; i<n; i++) {
			if(Math.abs(weights[i]) < minimumWeight) continue;
			int index = i + 1;
			if(index2Features.containsKey(index))
				fvs.put(index2Features.get(index), (double)weights[i]);
		}
	}
	
	@Override
	public String toString() {
		StringBuilder fv = new StringBuilder();
		fv.append(hostCode);
		fv.append(" ");
		fv.append(modelType);
		fv.append(" ");
		fv.append(sign);
		fv.append(" ");
		fv.append(rho);
		fv.append(" ");
		fv.append(probA);
		fv.append(" ");
		fv.append(probB);
		fv.append(" ");
		fv.append(version);
		fv.append(" ");
		fv.append(normalizationScheme);
		for (Map.Entry<String, Double> entry : fvs.entrySet()) {
			if(Math.abs(entry.getValue()) < minimumWeight) continue;
			fv.append(" ");
			fv.append(entry.getKey());
			fv.append(":");
			fv.append(String.format("%.4f", entry.getValue()));
		}
		return fv.toString();
	}

	public void saveModel(String modelFile) throws IOException {
		FileWriter outModel = new FileWriter(modelFile);
		
		outModel.write(toString());
		outModel.write('\n');
		outModel.flush();
		outModel.close();
	}
	
	public static void saveModel(String content, String modelFile) throws IOException {
		FileWriter outModel = new FileWriter(modelFile);
		outModel.write(content);
		outModel.write('\n');
		outModel.flush();
		outModel.close();
	}

	/**
	 * predict
	 * 
	 * iterates through the dataset and calculates the score of each instance within the dataset
	 * 
	 * @param dataset
	 * @param cutoff
	 * @param setProb
	 * @return
	 */
    public HashMap<String, Double> predict(DataSet dataset, double cutoff) {
    	HashMap<String, Double> scores = new HashMap<String, Double>(dataset.instances.size());
    	for (int i=0; i<dataset.instances.size(); i++) {
    		DataInstance instance = dataset.getDataInstance(i);
    		double score = predict(instance, cutoff);
    		if(score >= cutoff)
    			scores.put(instance.getName(), score);
    	}
    	return scores;
     }
    
    public double predict(DataInstance instance, double cutoff) {
		double score = computeRawScore(instance.getFvs());
		if(score < cutoff) return (double)-999.0; 
		return score;
	}

	public double predict(HashMap<String, Double> testFvs) {
		double score = computeRawScore(testFvs);
		return score;
	}

	public int getSign() {
		return sign;
	}

	public String getVersion() {
		return version;
	}

	public double getRho() {
		return rho;
	}

	public double getProbA() {
		return probA;
	}

	public double getProbB() {
		return probB;
	}

	public double getMinimumWeight() {
		return minimumWeight;
	}

	public HashMap<String, Double> getFvs() {
		return fvs;
	}

	public String getNormalizationScheme() {
		return normalizationScheme;
	}

	public String getHostCode() {
		return hostCode;
	}

	public String getModelType() {
		return modelType;
	}

	public double computeRawScore(HashMap<String, Double> testFvs) {
		double score = 0.0;
		for (Map.Entry<String, Double> entry : testFvs.entrySet()) {
			String feature = entry.getKey();
			if(fvs.containsKey(feature)) {
				double weight = fvs.get(feature);
				if(normalizationScheme == null || ! normalizationScheme.equalsIgnoreCase("binary"))
					weight *= entry.getValue();
				score += weight;
			}
		}

		score -= rho;
		score *= sign;

		if(! modelType.equalsIgnoreCase("OVA")) {
			score = DistributionEstimation.score2Probability(score, probA, probB);
		}

		return (double) score;
	}
}
