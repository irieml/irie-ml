package com.cipherone.engine.svm;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.GZIPInputStream;

import com.cipherone.engine.commons.PerformanceScore;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Collections2;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.google.common.collect.Sets;

public class OvaEvaluator {
	public static PerformanceScore collectEvaluateScores(DataSet dataset) {
		PerformanceScore evaluationScore = new PerformanceScore();
		for (int i=0; i<dataset.instances.size(); i++) {
			evaluationScore.update(dataset.getDataInstance(i).getPerformanceScore());
		}
		evaluationScore.completeScores();
		return evaluationScore;
	}

	/**
	 * filteringAssignments
	 * 
	 * @param candidates
	 * @param runDownRate
	 * @param threshold
	 * @param cutoff
	 * @return
	 */
	public static Multimap<String, CandidatePrediction> filteringAssignments(
			Multimap<String, CandidatePrediction> candidates, 
			final double threshold) {
		
		// returns candidate predictions indexed on codeNdx.
		ListMultimap<String, CandidatePrediction> ret = ArrayListMultimap.create();
		
		for (String noteId: candidates.keySet())
		{
			List<CandidatePrediction> noteCandidates = new ArrayList<CandidatePrediction>(candidates.asMap().get(noteId));
			if (noteCandidates.isEmpty()) continue;
			for (CandidatePrediction cp: noteCandidates)
			{
				if(cp.score < threshold) continue;
				ret.put(cp.id, cp);
			}
		}
		
		return ret;
	}

	
	public static HashMap<String, PerformanceScore> evaluatePerCode(DataSet dataset, Multimap<String,CandidatePrediction> assignments) {
		HashMultimap<String, DataInstance> codeToInstances = DataSet.getCodeToInstanceMultimap(dataset);
		Multimap<String, String> codeToNoteIds = Multimaps.transformValues(codeToInstances, DataInstance.toIdFunction);
		HashMap<String, PerformanceScore> evaluationScores = new  HashMap<String, PerformanceScore>();
		for (String code : codeToInstances.keySet()) {
			int trueCnt = codeToInstances.get(code).size();

			PerformanceScore codeScore = new PerformanceScore();
			if(! assignments.containsKey(code)) {
				codeScore.setTruePositives(0);
				codeScore.setFalseNegatives(trueCnt);
				codeScore.setFalsePositives(0);
				codeScore.setTrueNegatives(0);
			} else {
				Collection<CandidatePrediction> noteset = assignments.get(code);
				if(noteset == null || noteset.isEmpty()) {
					codeScore.setTruePositives(0);
					codeScore.setFalseNegatives(trueCnt);
					codeScore.setFalsePositives(0);
					codeScore.setTrueNegatives(0);
				} else {
					Set<String> matched = new HashSet<String>();
					for (CandidatePrediction note : noteset) {
						if(codeToNoteIds.get(code).contains(note.id)) {
							matched.add(note.id);
							codeScore.setTruePositives(codeScore.getTruePositives() + 1);
						} else {
							codeScore.setFalsePositives(codeScore.getFalsePositives() + 1);
						} 
					}
					for (String noteId : codeToNoteIds.get(code)) {
						if(! matched.contains(noteId)) {
							codeScore.setFalseNegatives(codeScore.getFalseNegatives() + 1);
						}
					}
				}
			}
			codeScore.completeScores();
			evaluationScores.put(code, codeScore);
		}
		
		return evaluationScores;
	}

	public static HashMap<String, PerformanceScore> evaluatePerCode(DataSet dataset) {
		return evaluatePerCode(dataset, DataSet.extractCandidates(dataset));
	}

	public static HashMap<String, PerformanceScore> evaluatePerNote(Iterable<DataInstance> dataset) {
		HashMap<String, PerformanceScore> evaluationScores = new  HashMap<String, PerformanceScore>();
		for (DataInstance instance : dataset){
			PerformanceScore individualScore = new PerformanceScore();
			if(! instance.getEngineCodes().isEmpty()) {
				Set<String> codes = Sets.newHashSet(Collections2.transform(instance.getEngineCodes(), CandidatePrediction.toCodeFunction()));
				individualScore = evaluateDataInstance(instance, codes);
			} else {
				individualScore = evaluateDataInstance(instance, null);
			}
			evaluationScores.put(instance.getName(), individualScore);
		}
		return evaluationScores;
	}

	public static HashMap<String, PerformanceScore> evaluatePerNote(DataSet dataset, Multimap<String, CandidatePrediction> assignments) {
		HashMap<String, PerformanceScore> evaluationScores = new  HashMap<String, PerformanceScore>(dataset.instances.size());
		for (int i=0; i<dataset.instances.size(); i++) {
			DataInstance instance = dataset.getDataInstance(i);
			PerformanceScore individualScore = new PerformanceScore();
			if(assignments.containsKey(instance.getId())) {
				Collection<CandidatePrediction> candidates = assignments.get(instance.getName());
				Set<String> codes = Sets.newHashSet(Collections2.transform(candidates, CandidatePrediction.toCodeFunction()));
				individualScore = evaluateDataInstance(instance, codes);
			} else {
				individualScore = evaluateDataInstance(instance, null);
			}
			evaluationScores.put(instance.getName(), individualScore);
		}
		return evaluationScores;
	}
	
	public static <T> PerformanceScore aggregatePerformance(HashMap<T, PerformanceScore> scores, Set<T> selected) {
		PerformanceScore overall = new PerformanceScore();
		for (T id : scores.keySet()) {
			if(selected == null || selected.contains(id)) {
				overall.update(scores.get(id));
			}
		}
		overall.completeScores();
		return overall;
	}

	public static PerformanceScore evaluateDataInstance(DataInstance instance, Set<String> assignments) {
		if(instance.getAnnotation() == null) return null;
		
		StringBuilder tpCodes = new StringBuilder();
		StringBuilder fpCodes = new StringBuilder();
		StringBuilder fnCodes = new StringBuilder();
		
		PerformanceScore evaScore = new PerformanceScore(); 
		if(assignments != null) {
			for (String code : instance.getAnnotation().keySet()) {
				if(assignments.contains(code)) {
					evaScore.setTruePositives(evaScore.getTruePositives() + 1);
					tpCodes.append(code + ",");
				} else {
					evaScore.setFalseNegatives(evaScore.getFalseNegatives() + 1);
					fnCodes.append(code + ",");
				}
			}
			for (String code : assignments) {
				if(! instance.getAnnotation().containsKey(code)) {
					evaScore.setFalsePositives(evaScore.getFalsePositives() + 1);
					fpCodes.append(code + ",");
				}
			}
		} else {
			evaScore.setTruePositives(0);
			evaScore.setFalsePositives(0);
			evaScore.setFalseNegatives(instance.getAnnotation().size());
			for (String code : instance.getAnnotation().keySet()) {
				fnCodes.append(code + ",");
			}
		}

		evaScore.completeScores();

		return evaScore;
	}
	
	public static PerformanceScore evaluateDataInstance(DataInstance instance) {
		return evaluateDataInstance(instance, Sets.newHashSet(Collections2.transform(instance.getEngineCodes(), CandidatePrediction.toCodeFunction())));
	}
	
	public static Collection<CandidatePrediction> readCandidateScores(String fileName, double threshold) throws IOException {
		InputStream in = null;
		if(fileName.endsWith(".gz")) {
			in = new GZIPInputStream(new FileInputStream(fileName));
		} else {
			in = new FileInputStream(fileName);
		}
		BufferedReader br = new BufferedReader(new InputStreamReader(in, "UTF-8"));


		List<CandidatePrediction> scores = Lists.newArrayList();
		
		String line = br.readLine();
		while(line != null) {
			String[] fields = line.split("\\t");
			line = br.readLine();

			if(fields.length != 3) continue;

			String noteName = fields[0];
			String codeName = fields[1];
			double score = Double.parseDouble(fields[2]);

			if(score >= threshold) {
				scores.add(new CandidatePrediction(codeName, noteName, score));
            }
		}
		
		br.close();
		
		return scores;
	}
	
	public static void updateEngineCodes(DataSet testset, Multimap<String, CandidatePrediction> noteAssignments) {
		Map<String, DataInstance> idToInstance = DataSet.createNoteIDToInstanceIndex(testset);
		for (String noteId : noteAssignments.asMap().keySet()) {
			DataInstance inst = DataSet.getDataInstanceByName(noteId, idToInstance);
			List<CandidatePrediction> scores = (List<CandidatePrediction>) noteAssignments.asMap().get(noteId);
			inst.engineCodes.addAll(scores);
			for (CandidatePrediction cp : scores) {
				if(cp.code.equalsIgnoreCase("GOOD"))
					inst.setRawScore(cp.score);
			}
		}
	}
	
	// Platt's binary SVM Probablistic Output: an improvement from Lin et al.
	public static double[] sigmoid_train(int l, double[] dec_values, int[] labels) {
    	double A, B;
    	double prior1=0, prior0 = 0;

    	for (int i=0; i<l; i++) {
    		if (labels[i] > 0) prior1 += 1;
    		else prior0 += 1;
    	}

		int max_iter=100;       // Maximal number of iterations
		double min_step=1e-10;  // Minimal step taken in line search
		double sigma=1e-12;     // For numerically strict PD of Hessian
		double eps=1e-5;
		double hiTarget=(prior1+1.0)/(prior1+2.0);
		double loTarget=1/(prior0+2.0);
		double[] t= new double[l];
		double fApB,p,q,h11,h22,h21,g1,g2,det,dA,dB,gd,stepsize;
		double newA,newB,newf,d1,d2;

		// Initial Point and Initial Fun Value
		A=0.0; B=Math.log((prior0+1.0)/(prior1+1.0));
		double fval = 0.0;

		for (int i=0; i<l; i++) {
			if (labels[i]>0) t[i]=hiTarget;
			else t[i]=loTarget;
			fApB = dec_values[i]*A+B;
			if (fApB>=0)
				fval += t[i]*fApB + Math.log(1+Math.exp(-fApB));
			else
				fval += (t[i] - 1)*fApB +Math.log(1+Math.exp(fApB));
		}
		
		double[] probAB = new double[4];
		
		for (int iter=0; iter<max_iter; iter++) {
			// Update Gradient and Hessian (use H' = H + sigma I)
			h11=sigma; // numerically ensures strict PD
			h22=sigma;
			h21=0.0;g1=0.0;g2=0.0;
			for (int i=0; i<l; i++) {
				fApB = dec_values[i]*A+B;
				if (fApB >= 0) {
					p=Math.exp(-fApB)/(1.0+Math.exp(-fApB));
					q=1.0/(1.0+Math.exp(-fApB));
				} else {
					p=1.0/(1.0+Math.exp(fApB));
					q=Math.exp(fApB)/(1.0+Math.exp(fApB));
				}
				d2=p*q;
				h11+=dec_values[i]*dec_values[i]*d2;
				h22+=d2;
				h21+=dec_values[i]*d2;
				d1=t[i]-p;
				g1+=dec_values[i]*d1;
				g2+=d1;
			}

			// Stopping Criteria
			if (Math.abs(g1)<eps && Math.abs(g2)<eps)
				break;

			// Finding Newton direction: -inv(H') * g
			det=h11*h22-h21*h21;
			dA=-(h22*g1 - h21 * g2) / det;
			dB=-(-h21*g1+ h11 * g2) / det;
			gd=g1*dA+g2*dB;

			stepsize = 1;           // Line Search
			while (stepsize >= min_step) {
				newA = A + stepsize * dA;
				newB = B + stepsize * dB;

				// New function value
				newf = 0.0;
				for (int i=0; i<l; i++) {
					fApB = dec_values[i]*newA+newB;
					if (fApB >= 0)
						newf += t[i]*fApB + Math.log(1+Math.exp(-fApB));
					else
						newf += (t[i] - 1)*fApB +Math.log(1+Math.exp(fApB));
				}
				// Check sufficient decrease
				if (newf<fval+0.0001*stepsize*gd) {
					A=newA;B=newB;fval=newf;
					break;
				} else {
					stepsize = stepsize / 2.0;
				}
			}

			if (stepsize < min_step) {
				System.out.println("Line search fails in two-class probability estimates");
				break;
			}

			if (iter>=max_iter)
				System.out.println("Reaching maximal iterations in two-class probability estimates");
			probAB[0]=A;probAB[1]=B;
		}
		
		return probAB;
	}
}
