package com.cipherone.engine.svm;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import de.bwaldvogel.liblinear.SolverType;

public class OVA_Parameters {
	String trainFileName = null;
	String testFileName = null;
	boolean DEBUG = false;
	boolean verbose = false;
	boolean saveModels = false;
	boolean featureChanges = false;
	boolean selectedFeatureOnly = false;
	boolean applyInstanceWeight = false;
	boolean prob = false;
	int minimumSize = 3;
	int sampleSize = 10;
	int negativeSize = -1;
	int positiveSize = -1;
	int ncv = -1;
	int nensemble = 1;
	double ensembleCutoff = (double)0.0;
	double p2nScale = (double)0.0;
	double threshold = 0.0;

	double robustnessCut = (double)0.9;
	String featureType = "all";
	String fileRoot = null;
	String version;
	String normalizationScheme = null;
	int svmSolver = 1;
	
	public static boolean useRandomSeed = false;
	public static int randomSeed = 20141007;
	String modelFileName = null;
	
	Options options = null;
	
	public void setOptions() {
		Option help = new Option( "help", "print this message" );
		Option version = new Option( "version", "print the version information and exit" );
		Option quiet = new Option( "quiet", "be extra quiet" );
		Option verbose = new Option( "verbose", "be extra verbose" );
		Option debug = new Option( "debug", "print debugging information" );
		Option useRandomSeed = new Option("useRandomSeed", "use random seed");
		Option savemodel = new Option( "saveModel", "save and integrate all trained models into a single model file" );
		Option fixedSeed = new Option( "fixedSeed", "Use predefined fixed seeds for various samplings" );
		Option applyInstanceWeight = new Option( "applyInstanceWeight", "Apply annotation confidence as instance training weight" );
		
		options = new Options();
		options.addOption(help);
		options.addOption(version);
		options.addOption(debug);
		options.addOption(useRandomSeed);
		options.addOption(verbose);
		options.addOption(quiet);
		options.addOption(savemodel);
		options.addOption(fixedSeed);
		options.addOption(applyInstanceWeight);
		
		options.addOption("t", "train", true, "This is the input training data set file");
		options.addOption("T", "test", false, "This is the input testing data set file");
		options.addOption("f", "featureType", false, "Selected feature subset used in training, \"all\": use all available features");
		options.addOption("cv", "cross-validation", false, "Set cross validations, n-folds");
		options.addOption("nensemble", "n-ensemble", false, "Set ensemble size");
		options.addOption("m", "minimum_size", false, "This is the minimum training exemplars for training an OVA model for the code");
		options.addOption("r", "robustness", false, "This is a double value to control SVM training robustness");
		options.addOption("rs", "randomSeed", false, "This is a random number as seed");
		options.addOption("N", "nNegatives", false, "This is to set maximum used negative instances in training");
		options.addOption("enCutoff", "ensembleCutoff", false, "minimum score kept during OVA prediction, which is relevent to ensemble and CV");
		options.addOption("ps", "positiveSize", false, "maximum number of positive instances");
		options.addOption("fnorm", "featureNormalization", false, "This is for feature vector normalization");
		options.addOption("s", "solverType", false, "This is SVM solver type option, default 1 for L2R_L2LOSS_SVC_DUAL, could be 5 for L1R_L2LOSS_SVC");
	}

	public void setupOVA(String[] args) {
		setOptions();
		CommandLineParser parser = new BasicParser();
		HelpFormatter formatter = new HelpFormatter();
		
		CommandLine line = null;
		try {
			line = parser.parse(options, args);
		} catch( ParseException exp ) {
	        formatter.printHelp("OVA", options);
	        System.err.println( "Parsing failed.  Reason: " + exp.getMessage() );
	    }
		
		if( line.hasOption( "help" ) ) {
			formatter.printHelp("OVA", options);
		} else {
			// required options
			trainFileName = line.getOptionValue('t');
			
			// optional options
			if(line.hasOption('T')) {
				testFileName = line.getOptionValue('T');
			}
			if(line.hasOption('f')) {
				featureType = line.getOptionValue('f');
			}
			if(line.hasOption('m')) {
				minimumSize = Integer.parseInt(line.getOptionValue('m'));
			}
			if(line.hasOption("ps")) {
				positiveSize = Integer.parseInt(line.getOptionValue("ps"));
			}
			if(line.hasOption("nNegatives")) {
				negativeSize = Integer.parseInt(line.getOptionValue("nNegatives"));
			}
			if(line.hasOption('r')) {
				robustnessCut = Double.parseDouble(line.getOptionValue('r'));
			}
			if(line.hasOption("rs")) {
				randomSeed = Integer.parseInt(line.getOptionValue("rs"));
			}
			if(line.hasOption("mf")) {
				modelFileName = line.getOptionValue("mf");
			}
			if(line.hasOption('s')) {
				svmSolver = Integer.parseInt(line.getOptionValue('s'));
				if(svmSolver == 0)
					Liblinear.solver = SolverType.L2R_LR;
				else if(svmSolver == 1)
					Liblinear.solver = SolverType.L2R_L2LOSS_SVC_DUAL;
				else if(svmSolver == 2)
					Liblinear.solver = SolverType.L2R_L1LOSS_SVC_DUAL;
				else if(svmSolver == 3)
					Liblinear.solver = SolverType.L2R_L1LOSS_SVC_DUAL;
				else if(svmSolver == 4)
					Liblinear.solver = SolverType.MCSVM_CS;
				else if(svmSolver == 5)
					Liblinear.solver = SolverType.L1R_L2LOSS_SVC;
				else if(svmSolver == 6)
					Liblinear.solver = SolverType.L1R_LR;
				else if(svmSolver == 7)
					Liblinear.solver = SolverType.L2R_LR_DUAL;
				else if(svmSolver == 11)
					Liblinear.solver = SolverType.L2R_L2LOSS_SVR;
				else if(svmSolver == 12)
					Liblinear.solver = SolverType.L2R_L2LOSS_SVR_DUAL;
				else if(svmSolver == 13)
					Liblinear.solver = SolverType.L2R_L1LOSS_SVR_DUAL;
				else
					Liblinear.solver = SolverType.L2R_L2LOSS_SVC_DUAL;
			}
			if(line.hasOption("cross-validation")) {
				ncv = Integer.parseInt(line.getOptionValue("cross-validation"));
			}
			if(line.hasOption("n-ensemble")) {
				nensemble = Integer.parseInt(line.getOptionValue("n-ensemble"));
			}
			if(line.hasOption("ensembleCutoff")) {
				ensembleCutoff = Double.parseDouble(line.getOptionValue("ensembleCutoff"));
			}
			if(line.hasOption("featureNormalization")) {
				normalizationScheme = line.getOptionValue("featureNormalization");
			}
			
			// boolean options
			verbose = line.hasOption("verbose");
			DEBUG = line.hasOption("debug");
			useRandomSeed = line.hasOption("useRandomSeed") || line.hasOption("rs");
			saveModels = line.hasOption("saveModel");
			SVMUtility.useFixedSeed = line.hasOption("fixedSeed");
			applyInstanceWeight = line.hasOption("applyInstanceWeight");
			
			if(prob)
				threshold = 0.5;
		}
	}
	
	public void setParameters(OVA_Parameters param) {
		p2nScale = param.p2nScale;
		robustnessCut = param.robustnessCut;
		sampleSize = param.sampleSize;
		featureType = param.featureType;
		ncv = param.ncv;
		nensemble = param.nensemble;
		version = param.version;
		threshold = param.threshold;
		
		verbose = param.verbose;
		DEBUG = param.DEBUG;
		saveModels = param.saveModels;
		minimumSize = param.minimumSize;
		negativeSize = param.negativeSize;
		positiveSize = param.positiveSize;
		ensembleCutoff = param.ensembleCutoff;
		normalizationScheme = param.normalizationScheme;
	}
	
	public static OVA_Parameters getDefault() {
    	OVA_Parameters param = new OVA_Parameters();
    	param.setEnsembleCutoff((double)-2.0);
    	param.setApplyInstanceWeight(false); // could be set to true
    	param.setNormalizationScheme("sqrt");
    	param.setFeatureType("ALL");
    	param.setSaveModels(false);
    	param.setVerbose(false);
    	param.setVersion("1.0.0");
    	param.setMinimumSize(1);
    	param.setSampleSize(5);
		
    	return param;
	}

	public boolean isDEBUG() {
		return DEBUG;
	}

	public void setDEBUG(boolean dEBUG) {
		DEBUG = dEBUG;
	}

	public boolean isVerbose() {
		return verbose;
	}

	public void setVerbose(boolean verbose) {
		this.verbose = verbose;
	}

	public boolean isSaveModels() {
		return saveModels;
	}

	public void setSaveModels(boolean saveModels) {
		this.saveModels = saveModels;
	}

	public boolean isFeatureChanges() {
		return featureChanges;
	}

	public void setFeatureChanges(boolean featureChanges) {
		this.featureChanges = featureChanges;
	}

	public int getMinimumSize() {
		return minimumSize;
	}

	public void setMinimumSize(int minimumSize) {
		this.minimumSize = minimumSize;
	}

	public int getSampleSize() {
		return sampleSize;
	}

	public void setSampleSize(int sampleSize) {
		this.sampleSize = sampleSize;
	}

	public int getNegativeSize() {
		return negativeSize;
	}

	public void setNegativeSize(int negativeSize) {
		this.negativeSize = negativeSize;
	}

	public int getPositiveSize() {
		return positiveSize;
	}

	public void setPositiveSize(int positiveSize) {
		this.positiveSize = positiveSize;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public int getNcv() {
		return ncv;
	}

	public void setNcv(int ncv) {
		this.ncv = ncv;
	}

	public double getEnsembleCutoff() {
		return ensembleCutoff;
	}

	public void setEnsembleCutoff(double ensembleCutoff) {
		this.ensembleCutoff = ensembleCutoff;
	}

	public double getP2nScale() {
		return p2nScale;
	}

	public void setP2nScale(double p2nScale) {
		this.p2nScale = p2nScale;
	}

	public double getRobustnessCut() {
		return robustnessCut;
	}

	public void setRobustnessCut(double robustnessCut) {
		this.robustnessCut = robustnessCut;
	}

	public String getFeatureType() {
		return featureType;
	}

	public void setFeatureType(String featureType) {
		this.featureType = featureType;
	}

	public String getTrainFileName() {
		return trainFileName;
	}

	public void setTrainFileName(String trainFileName) {
		this.trainFileName = trainFileName;
	}

	public String getTestFileName() {
		return testFileName;
	}

	public void setTestFileName(String testFileName) {
		this.testFileName = testFileName;
	}

	public String getFileRoot() {
		return fileRoot;
	}

	public void setFileRoot(String fileRoot) {
		this.fileRoot = fileRoot;
	}

	public String getNormalizationScheme() {
		return normalizationScheme;
	}

	public void setNormalizationScheme(String normalizationScheme) {
		this.normalizationScheme = normalizationScheme;
	}

	public boolean isSelectedFeatureOnly() {
		return selectedFeatureOnly;
	}

	public void setSelectedFeatureOnly(boolean selectedFeatureOnly) {
		this.selectedFeatureOnly = selectedFeatureOnly;
	}

	public int getNensemble() {
		return nensemble;
	}

	public void setNensemble(int nensemble) {
		this.nensemble = nensemble;
	}

	public boolean isApplyInstanceWeight() {
		return applyInstanceWeight;
	}

	public void setApplyInstanceWeight(boolean applyInstanceWeight) {
		this.applyInstanceWeight = applyInstanceWeight;
	}

	public boolean isProb() {
		return prob;
	}

	public void setProb(boolean prob) {
		this.prob = prob;
	}

	public double getThreshold() {
		return threshold;
	}

	public void setThreshold(double threshold) {
		this.threshold = threshold;
	}

	public static boolean isUseRandomSeed() {
		return useRandomSeed;
	}

	public static void setUseRandomSeed(boolean useRandomSeed) {
		OVA_Parameters.useRandomSeed = useRandomSeed;
	}

	public String getModelFileName() {
		return modelFileName;
	}

	public void setModelFileName(String modelFileName) {
		this.modelFileName = modelFileName;
	}
}
