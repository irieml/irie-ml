package com.cipherone.engine.svm;

import java.io.FileWriter;
import java.util.HashMap;

import com.cipherone.engine.commons.PerformanceScore;

public abstract class SVM implements Runnable {
	boolean DEBUG = false;
	boolean verbose = false;
	boolean saveModels = false;
	boolean testPipeline = false;
	int minimumSize = 3;
	int sampleSize = 30;
	int negativeSize = 5000;
	int positiveSize = -1;
	double cutoff = 0.0;
	double threshold = 0.0;
	double runDownRate = 0.7;
	double p2nScale = 0.0;

	double robustnessCut = 0.9;
	String featureType = "all";
	HashMap<String, Integer> featureIndexes = null;
	HashMap<String, String> modelFvs = null;
	
	DataSet trainDataSet = null;
	DataSet testDataSet = null;
	PerformanceScore scores = null;
	FileWriter out= null;
	String codeFamily;
	
	public SVM(String name, DataSet trainset, DataSet testset) {
		//code = name;
		trainDataSet = trainset;
		testDataSet = testset;
	}
	
	abstract public void run() ;
		/*
		runTraining(0);
		
		if(saveModels) {
			String modelFile = codeFamily + "_f-" + featureType  + ".model";
			createModelFile(modelFile);
			if(testPipeline) testModel(modelFile);
		}
	*/
	//}
}
