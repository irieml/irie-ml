package com.cipherone.engine.svm;


public interface PositiveNegativeInstances extends Iterable<DataInstance>{
	public int numberOfPositiveNotes();
	public int numberOfNegativeNotes();
	public int size();
}
