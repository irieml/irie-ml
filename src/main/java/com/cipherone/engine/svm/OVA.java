package com.cipherone.engine.svm;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

// import org.apache.log4j.Logger;

import com.cipherone.engine.commons.PerformanceScore;
import com.cipherone.engine.utils.DistributionEstimation;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Collections2;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multiset;
import com.google.common.collect.Multiset.Entry;
import com.google.common.collect.Sets;

import de.bwaldvogel.liblinear.Model;

public class OVA {
	// private static Logger log = Logger.getLogger(OVA.class);

	OVA_Parameters params = new OVA_Parameters();
	HashMap<String, Integer> validCodes = null;
	Predicate<String> validCodePredicate = Predicates.alwaysTrue();
	
	DataSet trainset = null;
	DataSet testset = null;
	BufferedWriter out = null;
	HashBiMap<String, Integer> indexmap = null;
	PerformanceScore performance;
	
	public HashBiMap<String, Integer> getIndexmap() {
		return indexmap;
	}

	public void setIndexmap(HashBiMap<String, Integer> indexmap) {
		this.indexmap = indexmap;
	}

	HashMap<String, ModelEngine> models = null;
	Multimap<String, CandidatePrediction> candidates = ArrayListMultimap.create();

	public OVA() {}
	
	public OVA(String[] args) {
		params.setupOVA(args);
	}
	
	public void initialize(OVA ova) {
		params.setParameters(ova.params);
	}

	public void setDataSets(DataSet trainset, DataSet testset) throws IOException {
		this.trainset = trainset;
		this.testset = testset;
		
		// finalize feature map based on trainset features
		if(indexmap == null)
			trainset.fixFeatureSpace();
		
		validCodes = trainset.getValidCodeSet(params.minimumSize, validCodePredicate);
		if(!validCodes.containsKey("GOOD")) {
			System.out.println("The dataset doesn't have enough " + "(<" + params.getMinimumSize() + ") positive training instances, terminated");
			System.exit(0);
		}
	}
	
	public void run() throws IOException {
		if(trainset == null)
			return;
		//Make sure we have enough for good and bad in the dataset
		if(validCodes == null)
			validCodes = trainset.getValidCodeSet(params.minimumSize, validCodePredicate);
				
		//Build the model
		runTraining();

		if(testset != null) {
			runPredicting();
		}			
	}

	/**
	 * countPositives
	 * 
	 * count how many positive examples are in a dataset
	 * 
	 * @param code
	 * @param code2Notes
	 * @return
	 */
	private int countPositives(String code, HashMultimap<String, DataInstance> code2Notes) {
    	int actualCount = 0;
		for (DataInstance instance: code2Notes.get(code)) {
    		int repeats = 1;
    		if(params.applyInstanceWeight) repeats = (int) instance.getWeight();
    		actualCount += repeats;
    	}
		return actualCount;
	}

	// loading SVM models into a hashmap is problematic when training CPT on 5000 positive / 500000 negative examples, as we exhaust memory.
	// FIXME: pass in "modelCollector", which could be a thin wrapper around a HashMap, or if memory needs to be conserved, 
	// it can write output incrementally, perhaps delaying the mapping from Strings->Ints until later...
	// Guoli: agree!
	/**
	 * runTraining
	 * 
	 * workhorse that actually builds the model
	 * 
	 */
	public void runTraining() {
		//we will have 2 models, good and bad
		models = new HashMap<String, ModelEngine>(validCodes.size());
		HashMultimap<String, DataInstance> code2Notes = DataSet.getCodeToInstanceMultimap(trainset);
		
		//Iterate through the valid codes, and build a model for each code
		for (String code : validCodes.keySet()) {
			if(!code.equalsIgnoreCase("GOOD")) continue;
			
			int minimumNegativeCount = countPositives(code, code2Notes) * 6;
			//cam uses all negatives
			int thisNegSize = params.negativeSize;
			if(thisNegSize > 0 && minimumNegativeCount > thisNegSize)
				thisNegSize = minimumNegativeCount;
			
			//finalize positive and negatives we want to use to build the model
			PositiveNegativeInstances currenttrainset = getTrainingSet(code, params.positiveSize, thisNegSize);
				
			//Cannot build if the trainset did not find enough examples
			if(currenttrainset != null) {
				//Pass in the examples and let the SVM magic happen
				Liblinear svm = new Liblinear();
				svm.setIndexmap(trainset.getIndexmap());
				Model linearModel = svm.train(currenttrainset, params, code);

				ModelEngine model = new ModelEngine(linearModel,
						params.getFileRoot(), 
						params.getNormalizationScheme(),
						"OVA",
						params.getVersion(),
						trainset.getIndexmap().inverse());
				
				//save the result of the model into the hashmap
				models.put(code, model);
			}
		}
	}

	/**
	 * runPredicting
	 * 
	 * @throws IOException
	 */
	// TODO: it looks like this function would be very expensive to run with, for instance, a 50,000 test set, as everything is in memory, by batch
	public void runPredicting() throws IOException {
		// System.out.println("RunPredicting...");
		if(models == null || models.isEmpty() || testset == null) return;
		
		//Empty the list - fresh start
		candidates.clear();
		
		//Iterate through the models we just built
		for (String code : models.keySet()) {
			// log.debug("RunPredicting...Code="+code);
			//Get the scores for this specific model
			HashMap<String, Double> scores = models.get(code).predict(testset, params.ensembleCutoff);
		
			//Add the scores into a list of scores per model
			for (Map.Entry<String, Double> e: scores.entrySet())
				candidates.put(code, new CandidatePrediction(code, e.getKey(), e.getValue()));
		}
		updateEngineCodes(OvaEvaluator.filteringAssignments(SVMUtility.indexOnNoteId(candidates.values()), params.threshold));
	}
	
	public void updateEngineCodes(Multimap<String, CandidatePrediction> noteAssignments) {
		OvaEvaluator.updateEngineCodes(testset,  noteAssignments);
	}
		
	public void setOutputFile(String outputFileName) {
		File output = new File(outputFileName);
		try {
			out = new BufferedWriter(new FileWriter(output));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static Set<String> getValidCodeSet(Iterable<DataInstance> instances, final int minSize) {
		Multiset<String> histogram = HashMultiset.create();
		for (DataInstance i: instances)
		{	
			for (String id: i.getAnnotation().keySet())
				histogram.add(id);
		}
		Set<Entry<String>> validEntries = Sets.filter(histogram.entrySet(), new Predicate<Multiset.Entry<String>>(){
			public boolean apply(Multiset.Entry<String> e){
				return e.getCount() >= minSize;
			}
		});
		return Sets.newHashSet(Collections2.transform(validEntries, new Function<Multiset.Entry<String>, String>(){
			public String apply(Entry<String> arg0) {
				return arg0.getElement();
			}}));
	}

	/**
	 * getTrainingSet
	 * 
	 * Makes sure we have at least the minimum number of positive and negative examples
	 * 
	 * @param code
	 * @param maxPositive
	 * @param maxNegative
	 * @return
	 */
	private PositiveNegativeInstances getTrainingSet(String code, int maxPositive, int maxNegative) {
		PositiveNegativeInstances pni = DataSet.getPositiveNegativeInstances(code, maxPositive, maxNegative, trainset.instances);
	
		if(pni.numberOfPositiveNotes() < params.minimumSize) {
			System.out.println(code + " has " + pni.numberOfPositiveNotes() + " positives, which is few than " + params.minimumSize + ", no model can be built");
			return null;
		} else if(pni.numberOfNegativeNotes() < params.minimumSize) {
			System.out.println(code + " has " + pni.numberOfNegativeNotes() + " negatives, which is few than " + params.minimumSize + ", no model can be built");
			return null;
		}
		
		return pni;
	}
	
	public double[] runCrossValidation() throws IOException {
		return runCrossValidation(params.ncv);
	}

	public double[] runCrossValidation(int ncv) throws IOException {
		List<DataInstance> instances = trainset.getInstances();
		if(SVMUtility.useFixedSeed) {
			Collections.shuffle(instances, new Random(891234567));
		} else {
			Collections.shuffle(instances);
		}
		//How many instances each subset will get
		int secSize = instances.size() / ncv;
		if(secSize * ncv < instances.size()) secSize ++;

		List<List<DataInstance>> secs  = Lists.partition(instances, secSize);
		HashMap<String, Double> scores = new HashMap<String, Double>();
		Multimap<String, CandidatePrediction> candidates = ArrayListMultimap.create();
		for (int i=0; i<secs.size(); i++) {
			List<DataInstance> trainInstances = Lists.newArrayList();
			List<DataInstance> testInstances = Lists.newArrayList();

			//Pick the subset we are iterating on for our test set, and use all the others as the training set
			testInstances.addAll(secs.get(i));
			for (int j=0; j< secs.size(); j++) {
				if(j != i) trainInstances.addAll(secs.get(j));
			}
			
			DataSet train = new DataSet(trainInstances);
			DataSet test = new DataSet(testInstances);

			if(params.DEBUG) {
				train.toAnnotatedInputFile("train_" + i + ".dat");
				test.toAnnotatedInputFile("test_" + i + ".dat");
			}

			OVA subova = new OVA();
			OVA_Parameters param = new OVA_Parameters();
			param.setParameters(params);
			param.setFileRoot(params.getFileRoot() + "-cv" + i);
			subova.setParams(param);
			subova.params.setNcv(i);
			subova.params.setVerbose(false);
			subova.trainset = train;
			subova.testset = test;
			
			subova.setDataSets(train, test);
			//clear out the scores from the possible previous run
			subova.testset.clearEngineCodes();
			//train the model
			subova.run();
			
			if(subova.models.containsKey("GOOD")) {
				scores.putAll(subova.models.get("GOOD").predict(subova.testset, (double)-99.9));
				candidates.putAll(OvaEvaluator.filteringAssignments(SVMUtility.indexOnNoteId(subova.candidates.values()), subova.params.threshold));
			} else {
				System.out.println("This cross validation run did not produce valid model because of small training size, the cross validation result may be contaminated");
			}
		}
		
		performance = OvaEvaluator.evaluatePerCode(trainset, SVMUtility.indexOnCode(candidates.values())).get("GOOD");
		
		//run probability estimation!!!
		double[] sigmoidParams = null;
		if(params.isProb()) {
			DistributionEstimation de = new DistributionEstimation();
			de.probabilityEstimate(trainset.getInstances());
			sigmoidParams = de.getParams().clone();
			de.tranform(trainset.getInstances());
		}
		
        return sigmoidParams;
	}
	
	public PerformanceScore getPerformance() {
		return performance;
	}

	public void setPerformance(PerformanceScore performance) {
		this.performance = performance;
	}

	public void closeOutput() throws IOException {
		out.close();
	}
	
	public void setValidCodeSet(HashMap<String, Integer> validCodeSet) {
		this.validCodes = new HashMap<String, Integer>();
		validCodes.putAll(validCodeSet);		
	}

	public OVA_Parameters getParams() {
		return params;
	}

	public void setParams(OVA_Parameters params) {
		this.params = params;
	}

	public HashMap<String, Integer> getValidCodes() {
		return validCodes;
	}

	public void setValidCodes(HashMap<String, Integer> validCodes) {
		this.validCodes = validCodes;
	}

	public HashMap<String, ModelEngine> getModels() {
		return models;
	}

	public void setModels(HashMap<String, ModelEngine> models) {
		this.models = models;
	}

	public Multimap<String, CandidatePrediction> getCandidates() {
		return candidates;
	}

	public void setCandidates(Multimap<String, CandidatePrediction> candidates) {
		this.candidates = candidates;
	}
}
