package com.cipherone.engine.svm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;

public class SVMUtility {
	public static boolean useFixedSeed = false;
	public static Multimap<String,CandidatePrediction> indexOnCode(Iterable<CandidatePrediction> candidates)
	{
		return Multimaps.index(candidates, CandidatePrediction.toCodeFunction());
	}
	
	public static Multimap<String,CandidatePrediction> indexOnNoteId(Iterable<CandidatePrediction> candidates)
	{
		return Multimaps.index(candidates, CandidatePrediction.toIDFunction());
	}

	public static Multimap<String, CandidatePrediction> scoreSwitch(Multimap<String, CandidatePrediction> notes) {
		return Multimaps.index(notes.values(), new Function<CandidatePrediction, String>(){
			public String apply(CandidatePrediction arg0) {
				return arg0.code;}
			});
	}
	
	public static <K, V extends Comparable<V>> List<K> sortByValue(Map<K,V> unsorted)
	{
		Comparator <Map.Entry<K, V>> comparator = new Comparator<Map.Entry<K, V>>() {
			public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
				return o2.getValue().compareTo(o1.getValue());
			}
		};
		ArrayList<Entry<K, V>> entryList = Lists.newArrayList(unsorted.entrySet());
		Collections.sort(entryList,	comparator);

		return Lists.newArrayList(Collections2.transform(entryList, new Function<Map.Entry<K,V>, K>(){
			public K apply(Map.Entry<K,V> e){ return e.getKey(); }
		}));
	}
			
	public static String filterFvs(String inputString, String featureType) {
		return filterFvs(inputString, featureType, null);
	}

	public static String filterFvs(String inputString, String featureType, Set<String>stopwords) {
		Pattern p = Pattern.compile(".*" + featureType + ".*");
		String[] fvs = inputString.split("\\s+");
		StringBuilder filtered = new StringBuilder();
		for (int i=0; i<fvs.length; i++) {
			if(featureType.equalsIgnoreCase("all") || p.matcher(fvs[i]).matches()) {
				if(stopwords == null || fvs[i].startsWith("P_")) {
					filtered.append(fvs[i] + " ");
				} else {
					int position = fvs[i].indexOf(':');
					String feature = fvs[i].substring(0, position);
					String[] segs = feature.split("_");
	
					String bare = StringUtils.join(Arrays.copyOfRange(segs, 4, segs.length), '_');
					String refined = StringUtils.join(Arrays.copyOfRange(segs, 2, segs.length), '_'); // include region and negation
	
					if(stopwords.contains(bare) || stopwords.contains(refined)) {
						continue;
					} else {
						filtered.append(fvs[i] + " ");
					}
				}
			}
		}
		
		return filtered.toString().trim();
	}

	public static String formFvs(String inputString, boolean update, HashBiMap<String,Integer> featureMap) {
		if(inputString == null || inputString.trim().isEmpty())
			return null;
		String[] fvs = inputString.split("\\s+");
		
		ArrayList<String> usedFvs = new ArrayList<String>(fvs.length);
		
		for (int i=0; i<fvs.length; i++) {
			String[] pair = fvs[i].split("\\:");
			if(update || featureMap.containsKey(pair[0])) {
				usedFvs.add(fvs[i]);
			}
		}
		
		if(usedFvs.isEmpty()) {
			return null;
		} else {
			return StringUtils.join(usedFvs.toArray(), ' ');
		}
	}
	
	public static String normalizeFvs(String inputString, String scheme) {
		if(scheme == null) {
			return inputString;
		} else {
			if(inputString == null || inputString.trim().isEmpty())
				return inputString;
			String[] fvs = inputString.split("\\s+");
			
			HashMap<String, Double> maximums = new HashMap<String, Double>();
			for (int i=0; i<fvs.length; i++) {
				int position = fvs[i].lastIndexOf(':');
				if(position < 0)
					System.out.println(fvs[i] + " --- bad feature");
				String feature = fvs[i].substring(0, position).replaceAll(":", "-");
				int pos = feature.indexOf('_');
				String featureType = "ALL";
				if(pos > 0)
					featureType = feature.substring(0, pos);

				double count = 0;
				try {
					count = Double.parseDouble(fvs[i].substring(position + 1));
				} catch (NumberFormatException e) {
					System.out.println(fvs[i]);
				}
				
				if(maximums.containsKey(featureType)) {
					double current = maximums.get(featureType);
					if(current < count) maximums.put(featureType, count);
				} else {
					maximums.put(featureType, count);
				}
			}
	
			StringBuilder outputString = new StringBuilder();
			for (int i=0; i<fvs.length; i++) {
				int position = fvs[i].lastIndexOf(':');
				String feature = fvs[i].substring(0, position).replaceAll(":", "-");
				int pos = feature.indexOf('_');
				String featureType = "ALL";
				if(pos > 0)
					featureType = feature.substring(0, pos);

				double count = Double.parseDouble(fvs[i].substring(position + 1));
				
				if(scheme.equalsIgnoreCase("binary")) {
					outputString.append(feature + ":1.0 ");
				} else if(scheme.equalsIgnoreCase("sqrt")) {
					outputString.append(feature + ":" + Math.sqrt(count / maximums.get(featureType)) + " ");
				} else if(scheme.equalsIgnoreCase("augment")) {
					outputString.append(feature + ":" + 0.5 * (1.0 + count / maximums.get(featureType)) + " ");
				} else {
					System.out.println("Invalid normalized scheme " + scheme);
				}
			}
			return outputString.toString().trim();
		}
	}
	
	public static HashMap<String, Double> normalizeFvs(HashMap<String, Double> input, String scheme) {
		if(scheme == null || input == null || input.isEmpty()) {
			return input;
		} else {
			HashMap<String, Double> maximums = new HashMap<String, Double>();
			for (Map.Entry<String, Double> entry : input.entrySet()) {
				String feature = entry.getKey();
				int position = feature.indexOf('_');
				String featureType = "ALL";
				if(position > 0)
					featureType = feature.substring(0, position);
				double count = entry.getValue();
				
				if(maximums.containsKey(featureType)) {
					double current = maximums.get(featureType);
					if(current < count) maximums.put(featureType, count);
				} else {
					maximums.put(featureType, count);
				}
			}
	
			HashMap<String, Double> output = new HashMap<String, Double>();
			for (Map.Entry<String, Double> entry : input.entrySet()) {
				String feature = entry.getKey();
				int position = feature.indexOf('_');
				String featureType = "ALL";
				if(position > 0) {
					featureType = feature.substring(0, position);
					
				if(scheme.equalsIgnoreCase("binary")) {
					output.put(feature, 1.0);
				} else if(scheme.equalsIgnoreCase("sqrt")) {
					output.put(feature, Math.sqrt(entry.getValue() / maximums.get(featureType)));
				} else if(scheme.equalsIgnoreCase("augment")) {
					output.put(feature, 0.5 * (1.0 + entry.getValue() / maximums.get(featureType)));
				} else {
					System.out.println("Invalid normalized scheme " + scheme);
				}
				}
			}
			return output;
		}
	}

}
