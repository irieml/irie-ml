package com.cipherone.engine.svm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;

/**
 * this class is for data driven SVM hyper-parameter computation, it's basically an implementation of approaches published by Oracle SVM team.
 * the paper can be found at http://www.oracle.com/technetwork/database/options/advanced-analytics/odm/overview/support-vector-machines-paper-1205-129825.pdf
 * 
 * @author Guoli Wang
 *
 */
public class SVMParameters {

	/**
	 * these properties are initialized by the most likely default values. all can be assigned specific values by set functions.
	 */
	double[][] pairwiseScores;
	double robustness;
	double p2nScale;
	private PositiveNegativeInstances pni;
	List<DataInstance> dataset = Lists.newArrayList();
	List<Integer> positiveIndexes = Lists.newArrayList();
	List<Integer> negativeIndexes = Lists.newArrayList();

	public SVMParameters() {
		robustness = 0.9;
		p2nScale = 0;
	}

	public SVMParameters(double _robustness, double _p2nScale, PositiveNegativeInstances pni) {
		robustness = _robustness;
		p2nScale = _p2nScale;
		this.pni = pni;
		initIndexesAndDataSet(pni);
	}

	private void initIndexesAndDataSet(PositiveNegativeInstances pni2) {
		dataset = Lists.newArrayList(pni.iterator());
		// no need for shuffling
		// Collections.shuffle(dataset);
		for (int i = 0; i < dataset.size(); i++)
		{
			if (dataset.get(i).getLabel() == 1) positiveIndexes.add(i);
			else negativeIndexes.add(i);
		}
		Preconditions.checkArgument(positiveIndexes.size() == negativeIndexes.size(), "Num pos:" + positiveIndexes.size() + ", neg=" + negativeIndexes.size());
		Preconditions.checkArgument(dataset.size() > 0 && positiveIndexes.size() > 0 && negativeIndexes.size() > 0);
		
	}

	public double getCValue()
	{
		if(dataset == null || dataset.isEmpty()) return 1.0;
		
		computePairwiseScores();
		ArrayList<Double> candidateList = new ArrayList<Double>();
		
		for (int i=0; i<positiveIndexes.size(); i++) {
			int index1 = positiveIndexes.get(i);
			double targetValue = 0.0;
			for (int j=0; j<positiveIndexes.size(); j++) {
				int index2 = positiveIndexes.get(j);
				targetValue += pairwiseScores[index1][index2];
			}
			for (int j=0; j<negativeIndexes.size(); j++) {
				int index2 = negativeIndexes.get(j);
				targetValue -= pairwiseScores[index1][index2];
			}
			
			if(targetValue > 0.0) {
				candidateList.add(1.0 / targetValue);
			}
		}
		for (int i=0; i<negativeIndexes.size(); i++) {
			int index1 = negativeIndexes.get(i);
			double targetValue = 0.0;
			for (int j=0; j<positiveIndexes.size(); j++) {
				int index2 = positiveIndexes.get(j);
				targetValue += pairwiseScores[index1][index2];
			} 
			for (int j=0; j<negativeIndexes.size(); j++) {
				int index2 = negativeIndexes.get(j);
				targetValue -= pairwiseScores[index1][index2];
			}
			
			if(targetValue < 0.0) {
				candidateList.add(-1.0 / targetValue);
			}
		}
		
		Collections.sort(candidateList);
		int index = (int) (robustness * candidateList.size());
		if(index < 0 || index >= candidateList.size()) {
			System.out.println("size " + candidateList.size() + " try to get " + index);
			return 1.0;
		} else {
			return candidateList.get(index);
		}
	}

	private void computePairwiseScores() {
		pairwiseScores = new double[dataset.size()][dataset.size()];
		for (int i=0; i<dataset.size(); i++) {
			DataInstance inst1 = dataset.get(i);
			for (int j=i; j<dataset.size(); j++) {
				DataInstance inst2 = dataset.get(j);
				pairwiseScores[i][j] = linearKernel(inst1.getFvs(), inst2.getFvs());
				pairwiseScores[j][i] = pairwiseScores[i][j];
			}
		}
	}
		
	/**
	 * internal function
	 * 
	 * @param fv1
	 * @param fv2
	 * @return
	 */
	private double linearKernel(HashMap<String, Double> fv1, HashMap<String, Double> fv2) {
		double score = 0.0;
	
		for (Map.Entry<String, Double> entry : fv1.entrySet()) {
			String key = entry.getKey();
			if(fv2.containsKey(key)) {
				score += entry.getValue() * fv2.get(key);
			}
		}
		
		return score;
	}
	
		
	public void setRobustness(double cut) {
		robustness = cut;
	}
	
	public double getRobustness() {
		return robustness;
	}
	
	public void setP2nScale(double scale) {
		p2nScale = scale;
	}
	
	public double getP2nScale() {
		return p2nScale;
	}
}
