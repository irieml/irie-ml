package com.cipherone.engine.svm;

import com.google.common.base.Function;

public class CandidatePrediction implements Comparable<CandidatePrediction> {

	final String id, code;
	double score;
	
	public CandidatePrediction(String code, String id, double score){
		this.code = code;
		this.id = id;
		this.score = score;
	}

	public static Function<CandidatePrediction, String> toCodeFunction() {
		return new Function<CandidatePrediction, String>(){
			public String apply(CandidatePrediction arg0) { return arg0.code ;}
		};
	}
	public static Function<CandidatePrediction, String> toIDFunction() {
		return new Function<CandidatePrediction, String>(){
			public String apply(CandidatePrediction arg0) { return arg0.id ;}
		};
	}
	
	public int compareTo(CandidatePrediction aThat) {
		if(aThat.score > score) 
			return -1;
		else if(aThat.score < score)
			return 1;
		else
			return 0;
	}

	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}

	public String getId() {
		return id;
	}

	public String getCode() {
		return code;
	}
}
