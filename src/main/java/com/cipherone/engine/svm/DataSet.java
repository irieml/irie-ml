package com.cipherone.engine.svm;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.zip.GZIPInputStream;

import com.cipherone.engine.commons.PerformanceScore;
import com.google.common.base.Predicate;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;

public class DataSet implements Iterable<DataInstance>{
	public static class InMemoryPositiveNegativeInstances implements PositiveNegativeInstances {
		ImmutableList<DataInstance> pos, neg, randomlyMixedSet;
		InMemoryPositiveNegativeInstances(List<DataInstance> pos, List<DataInstance> neg ){
			this.pos = ImmutableList.copyOf(pos);
			this.neg = ImmutableList.copyOf(neg);
			ArrayList<DataInstance> combined = Lists.newArrayList(pos);
			combined.addAll(neg);
			//Collections.shuffle(combined);
	    	if (OVA_Parameters.useRandomSeed) {
	    		Collections.shuffle(combined, new Random(OVA_Parameters.randomSeed));
	    	} else {
	    		Collections.shuffle(combined);
	    	}
			randomlyMixedSet = ImmutableList.copyOf( combined );
		}
		//@Override
		public Iterator<DataInstance> iterator() {
			return randomlyMixedSet.iterator();
		}
		public int numberOfPositiveNotes() {
			return pos.size();
		}
		public int numberOfNegativeNotes() {
			return neg.size();
		}
		
		public int size() { return randomlyMixedSet.size(); }

	}
	public static class TrainingCorpusSubset{
		ImmutableList<DataInstance> instances;
		final int posCnt, negCnt;
		public TrainingCorpusSubset(List<DataInstance> instances, int posCnt, int negCnt)
		{
			this.instances = ImmutableList.copyOf(instances);
			this.posCnt = posCnt;
			this.negCnt = negCnt;
		}
	}
	
	String sourceFileName;
	public String getSourceFileName() {
		return sourceFileName;
	}

	public void setSourceFileName(String sourceFileName) {
		this.sourceFileName = sourceFileName;
	}

	List<DataInstance> instances = new ArrayList<DataInstance>();
	PerformanceScore evaluationScore = null;
	Set<String> selectedFeatures = null;
	HashBiMap<String,Integer> indexmap = HashBiMap.create(new HashMap<String,Integer>());

    String topic;
    String model_name;
    String customer;
    String modelVersion;
    public String getModelVersion() {
		return modelVersion;
	}

	public void setModelVersion(String version) {
		this.modelVersion = version;
	}

    public DataSet() {}
	
	public HashBiMap<String, Integer> getIndexmap() {
		return indexmap;
	}

	public void setIndexmap(HashBiMap<String, Integer> indexmap) {
		this.indexmap = indexmap;
	}

	/**
	 * setIndexmap
	 * 
	 * define the feature space
	 * iterates through all feature vectors and finds all distinct features
	 * map of real feature string to integer
	 * 
	 * @param model
	 */
	public void setIndexmap(ModelEngine model) {
		indexmap.clear();
		for (String feature : model.fvs.keySet())
			updateFeatureSpace(feature);
	}

	public DataSet(Iterable<DataInstance> instances) {
		this.instances = Lists.newArrayList(instances);
	}
		
	public void toAnnotatedInputFile(String fileName) throws IOException {
		toAnnotatedInputFile(fileName, this);
	}
	public static void toAnnotatedInputFile(String fileName, Iterable<DataInstance> instances) throws IOException {
		BufferedWriter out = new BufferedWriter(new FileWriter(fileName));

		for (DataInstance i: instances) {
			out.write(i.toAnnotatedFVString());
		}
		out.close();
	}
	
	public InMemoryPositiveNegativeInstances getPositiveNegativeInstances(String code, int maxPositive, int maxNegative) {
		return DataSet.getPositiveNegativeInstances(code, maxPositive, maxNegative, this.instances);
	}

    private static <T> List<T> shuffle(Iterable<T> iterable) {
    	List<T> list = Lists.newArrayList(iterable);
    	if (OVA_Parameters.useRandomSeed) {
    		Collections.shuffle(list, new Random(OVA_Parameters.randomSeed));
    	} else {
    		Collections.shuffle(list);
    	}
    	return list;
    }

	public static InMemoryPositiveNegativeInstances getPositiveNegativeInstances(String code, int maxPositive, int maxNegative, Iterable<DataInstance> instances, Set<String> preSelected) {
		List<DataInstance> pos = Lists.newArrayList();
		List<DataInstance> neg = Lists.newArrayList();
		
		if(maxPositive < 0) 
			maxPositive = Integer.MAX_VALUE;
		if(maxNegative < 0)
			maxNegative = Integer.MAX_VALUE;

		if(preSelected != null) {
			Map<String,DataInstance> noteId2Instance = DataSet.createNoteIDToInstanceIndex(instances);
			for (String noteName : shuffle(preSelected)) {
				DataInstance instance = DataSet.getDataInstanceByName(noteName, noteId2Instance);
				if(instance.getAnnotation().containsKey(code)) {
					instance.setLabel(-1);
					instance.setWeight(1.0);
					neg.add(instance);
				}
				
				if (neg.size() >= maxNegative)
					break;
			}
		}
		for (DataInstance instance : shuffle(instances)){
			boolean isPositive = instance.getAnnotation().containsKey(code);
			if(isPositive && pos.size() < maxPositive) {
				instance.setLabel(1);
				instance.setWeight(instance.getAnnotation().get(code));
				pos.add(instance);
			} else if (!isPositive && neg.size() < maxNegative){
				if(preSelected == null || !preSelected.contains(instance)) {
					instance.setLabel(-1);
					instance.setWeight(1.0);
					neg.add(instance);
				}
			}
			if (pos.size() >= maxPositive && neg.size() >= maxNegative)
				break;
		}
		// System.out.println("getPositiveNegativeInstances...Done...pos=" +pos.size() + ", neg="+neg.size());
		return new InMemoryPositiveNegativeInstances(pos, neg);
	}

	public static InMemoryPositiveNegativeInstances getPositiveNegativeInstances(String code, int maxPositive, int maxNegative, Iterable<DataInstance> instances) {
		return getPositiveNegativeInstances(code, maxPositive, maxNegative, instances, null);	
	}

	public void fixFeatureSpace() {
		indexmap.clear();
		for (DataInstance instance : instances) {
			for (String feature : instance.getFvs().keySet())
				updateFeatureSpace(feature);
		}
	}

	private void updateFeatureSpace(String feature) {
		if(! indexmap.containsKey(feature)) indexmap.put(feature, indexmap.size() + 1); 
	}
	
	public void addAll(Iterable<DataInstance> dataset) {
		instances.addAll(Lists.newArrayList(dataset));
	}
	
	public void add(DataInstance inst) {
		instances.add(inst);
	}

	//FIXME not exact random replicate sampling, but can be used for now
	public static PositiveNegativeInstances drawSubset(String code, int nPositives, int nNegatives, PositiveNegativeInstances wholeCorpus) {
		PositiveNegativeInstances pni = DataSet.getPositiveNegativeInstances(code, nPositives, nNegatives, wholeCorpus);
		List<DataInstance> posInstances = Lists.newArrayList();
		List<DataInstance> negInstances = Lists.newArrayList();
		
		Iterator <DataInstance> itr = pni.iterator();
		while (itr.hasNext())
		{
			DataInstance di = itr.next();
			if (di.getLabel() == 1) posInstances.add(di);
			else negInstances.add(di);
		}
			
		while (posInstances.size() < nPositives)
		{
			int cnt = Math.min(posInstances.size(), nPositives-posInstances.size());
			List<DataInstance> copies = shuffle(posInstances).subList(0, cnt);
			posInstances.addAll(copies);
		}

		while (negInstances.size() < nNegatives)
		{
			int cnt = Math.min(negInstances.size(), nNegatives-negInstances.size());
			List<DataInstance> copies = shuffle(negInstances).subList(0, cnt);
			negInstances.addAll(copies);
		}
		
		return new InMemoryPositiveNegativeInstances(posInstances, negInstances);
	}
	
	// FIXME: rename getCodeToInstanceMultimap
	public static HashMultimap<String, DataInstance> getCodeToInstanceMultimap(Iterable<DataInstance> instances) {
		HashMultimap<String, DataInstance> ret = HashMultimap.create();
		for (DataInstance i: instances)
			for (String code: i.getAnnotation().keySet())
				ret.put(code, i);
		return ret;
	}
	
	public void clearEngineCodes() {
		for (DataInstance instance : instances) {
			instance.setEngineCodes();
		}
	}
		
	public static Map<String,DataInstance> createNoteIDToInstanceIndex(Iterable<DataInstance> dataset)
	{
		return Maps.uniqueIndex(dataset, DataInstance.toIdFunction);
	}

	// FIXME: maybe should require that idIndexes is passed into function
	public void copyEngineCodes(Iterable<DataInstance> dataset,Map<String, DataInstance> idIndexes) {
		//Map<Integer, DataInstance> idIndexes = createNoteIDToInstanceIndex(this);
		for (DataInstance instance : dataset) {
			if(idIndexes.containsKey(instance.getId())) {
				if(! instance.getEngineCodes().isEmpty()) {
					getDataInstanceByName(instance.getName(), idIndexes).setEngineCodes(instance.getEngineCodes());
				}
			}
		}
	}
	
	public static Multimap<String, CandidatePrediction> extractCandidates (Iterable<DataInstance> instances) {
		Multimap<Integer, CandidatePrediction> candidates = ArrayListMultimap.create();
		for (DataInstance instance : instances) {
			candidates.putAll(instance.getId(), instance.getEngineCodes());
		}
		return SVMUtility.indexOnCode(candidates.values());
	}
	
	public List<DataInstance> getInstances() {
		return instances;
	}
	
	public DataInstance getDataInstance(int index) {
		return instances.get(index);
	}
	
	public static DataInstance getDataInstanceByName(String noteName, Map<String, DataInstance> idIndexes) {
		if(idIndexes.containsKey(noteName)) {
			return idIndexes.get(noteName);
		} else {
			return null;
		}
	}
	
	public Iterator<DataInstance> iterator() {
		return instances.iterator();
	}

	public HashMap<String, Integer> getValidCodeSet(int minimumSize, Predicate<String> validCodePredicate) {
		HashMap<String, Integer> validCodes = new HashMap<String, Integer>();
		HashMultimap<String, DataInstance> code2Notes = DataSet.getCodeToInstanceMultimap(this);
		for (String code : code2Notes.keySet()) {
			int size = code2Notes.get(code).size();
			if( size >= minimumSize && validCodePredicate.apply(code)) validCodes.put(code, size);
		}
		return validCodes;
	}

	public Set<String> getSelectedFeatures() {
		return selectedFeatures;
	}

	public void setSelectedFeatures(Set<String> selectedFeatures) {
		this.selectedFeatures = selectedFeatures;
	}

	public PerformanceScore getEvaluationScore() {
		return evaluationScore;
	}

	public String getModel_name() {
		return model_name;
	}

	public String getCustomer() {
		return customer;
	}

	public void setInstances(List<DataInstance> instances) {
		this.instances = instances;
	}

	public void setEvaluationScore(PerformanceScore evaluationScore) {
		this.evaluationScore = evaluationScore;
	}

	public void setModel_name(String _name) {
		this.model_name = _name;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}
}