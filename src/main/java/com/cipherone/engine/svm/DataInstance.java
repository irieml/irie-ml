package com.cipherone.engine.svm;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.cipherone.engine.commons.PerformanceScore;
import com.google.common.collect.Lists;
import com.google.common.base.Function;

public class DataInstance {
	private int label;

	private int assignment;
	private boolean primary;
	private String hostCode;
	private String version;
	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getHostCode() {
		return hostCode;
	}

	public void setHostCode(String hostCode) {
		this.hostCode = hostCode;
	}

	public boolean isPrimary() {
		return primary;
	}

	public void setPrimary(boolean primary) {
		this.primary = primary;
	}

	private double weight = 1.0;
	private int id = -1;
	private String name;

	List<CandidatePrediction> engineCodes = Lists.newArrayList();
	private HashMap<String, Double> annotations = null;   // what is this?
	public static Function<DataInstance, String> toIdFunction = new Function<DataInstance,String>(){ 
		public String apply(DataInstance i){ return i.name; } 
	};

	double rawScore = -999.9;
	private double confidence = -999.9;
	private PerformanceScore testPerformanceScore = null;
	private HashMap<String, Double> fvs = null;
	
	public DataInstance() {
		testPerformanceScore = new PerformanceScore();
		label = 0;
		weight = 1.0;
		//engineCodes = ArrayListMultimap.create();
	}
	
	public PerformanceScore getPerformanceScore()
	{
		return testPerformanceScore;
	}
	
	public void loadFvs(String fvString) {
		String[] features = fvString.split("\\s+");
		fvs = new HashMap<String, Double>(features.length);
		for (int i=0; i<features.length; i++) {
			if(! features[i].contains(":")) continue;
			String[] fields = features[i].split("\\:");
			fvs.put(fields[0], Double.parseDouble(fields[1]));
		}
	}
	
	public void loadInstance(String fvString) {
		String[] features = fvString.split("\\s+");
		label = Integer.parseInt(features[0]);
		weight = 1.0;
		int endIndex = features.length - 1;
		name = features[endIndex];
		loadFvs(StringUtils.join(Arrays.copyOfRange(features, 1, endIndex - 1), " "));
	}

	public HashMap<String, Double> filtering(Set<String> selected) {
		if(selected != null) {
			HashMap<String, Double> cleaned = new HashMap<String, Double>(selected.size());
			for (String feature : fvs.keySet()) {
				if(selected.contains(feature)) {
					cleaned.put(feature, fvs.get(feature));
				}
			}
			return cleaned;
		} else {
			return fvs;
		}
	}

	public String toPredictionOutputLine() {
		return name + " " + assignment + " " + confidence + System.getProperty("line.separator");
	}

	@Override
	public DataInstance clone() {
		DataInstance instance = new DataInstance();
		instance.setLabel(label);
		instance.setId(id);
		instance.setAnnotation(annotations);
		instance.setFvs(fvs);
		instance.setWeight(weight);
		return instance;
	}

	public String toAnnotatedFVString() {
		StringBuilder fvString = new StringBuilder();
		fvString.append(name);
		fvString.append('\t');
		for(Map.Entry<String, Double> entry : annotations.entrySet()) {
			fvString.append(" " + entry.getKey() + ":" + String.format("%.1f", entry.getValue()));
		}
		
		fvString.append('\t');
		fvString.append(toFVString());
		fvString.append('\n');
		
		return fvString.toString();
	}

	public String toFVString(Set<String> selectedFeatures) {
		StringBuilder fvString = new StringBuilder();
		for(Map.Entry<String, Double> entry : fvs.entrySet()) {
			if(selectedFeatures == null || selectedFeatures.contains(entry.getKey()))
				fvString.append(" " + entry.getKey() + ":" + String.format("%6.4f", entry.getValue()));
		}
		
		return fvString.toString().trim();
	}

	public String toFVString() {
		return toFVString(null);
	}

	public void setLabel(int value) {
		label = value;
	}
	
	public int getLabel() {
		return label;
	}
	
	public void setWeight(double weight2) {
		weight = weight2;
	}

	public double getWeight() {
		return weight;
	}
	
	public void setAnnotation(Set<String> list) {
		annotations = new HashMap<String, Double>(list.size());
		for (String category : list) {
			annotations.put(category, (double)1.0);
		}
	}
	
	public void setAnnotation(HashMap<String, Double> anns) {
		annotations = new HashMap<String, Double>(anns.size());
		annotations.putAll(anns);
	}
	
	public HashMap<String, Double> getAnnotation() {
		return annotations;
	}

	public void setEngineCodes() {
		engineCodes = Lists.newArrayList();
	}

	public void setEngineCodes(Collection<CandidatePrediction> anns) {
		engineCodes = Lists.newArrayList(anns);
	}
	
	public List<CandidatePrediction> getEngineCodes() {
		return engineCodes;
	}
	
	public int getAssignment() {
		return assignment;
	}
	
	public void setAssignment(int value) {
		assignment = value;
	}

	public void setId(int name) {
		id = name;
	}
	
	public int getId() {
		return id;
	}
		
	public void setConfidence(double prob) {
		confidence = prob;
	}
	
	public double getConfidence() {
		return confidence;
	}
	
	public void setRawScore(double value) {
		rawScore = value;
	}
	
	public double getRawScore() {
		return rawScore;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String _name) {
		name = _name;
	}

	public void setFvs(HashMap<String, Double> _fvs) {
		fvs = _fvs;
	}

	public void copyFvs(HashMap<String, Double> _fvs) {
		fvs = new HashMap<String, Double>();
		fvs.putAll(_fvs);
	}
	
	public HashMap<String, Double> getFvs() {
		return fvs;
	}

	public void addEngineCandidatePredication(CandidatePrediction cp) {
		engineCodes.add(cp);
	}
}
