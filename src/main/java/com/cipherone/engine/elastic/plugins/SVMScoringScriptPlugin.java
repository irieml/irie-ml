package com.cipherone.engine.elastic.plugins;

import java.util.Map;

import org.elasticsearch.plugins.Plugin;
import org.elasticsearch.script.AbstractDoubleSearchScript;
import org.elasticsearch.script.ExecutableScript;
import org.elasticsearch.script.NativeScriptFactory;
import org.elasticsearch.script.ScriptModule;

public class SVMScoringScriptPlugin extends Plugin {
	@Override
    public String name() {
        return "SVMScoring";
    }
    @Override
    public String description() {
        return "SVMScoring takes built SVM model to screen document on stored feature vector";
    }
    public void onModule(ScriptModule scriptModule) {
        scriptModule.registerScript(name(), SVMScoringScriptFactory.class);
    }

    public static class SVMScoringScriptFactory implements NativeScriptFactory {
		public ExecutableScript newScript(Map<String, Object> params) {
			return new SVMScoringScript(params);
		}

		public boolean needsScores() {
			// TODO Auto-generated method stub
			return false;
		}
    }

    public static class SVMScoringScript extends AbstractDoubleSearchScript {
    	Map<String, Object> params;
    	public SVMScoringScript(Map<String, Object> _params) {
    		params = _params;
    	}
    	
        @Override
        public double runAsDouble() {
            double a = 1;
            double b = 1;
            return a * b;
        }
    }
    
    /*
     * curl -XPOST localhost:9200/_search -d '{
  "query": {
    "function_score": {
      "query": {
        "match": {
          "body": "foo"
        }
      },
      "functions": [
        {
          "script_score": {
            "script": {
                "id": "my_script",
                "lang" : "native"
            }
          }
        }
      ]
    }
  }
}'
     */

}
